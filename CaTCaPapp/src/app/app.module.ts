import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NotificationComponent } from './components/notification/notification.component';
import { CredentialsInterceptor } from './interceptors/credentials/credentials.interceptor';
import { ConfirmDeleteAccountComponent } from './components/confirm-delete-account/confirm-delete-account.component';
import { ForgottenPasswordComponent } from './components/forgotten-password/forgotten-password.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from './components/register/register.component';
import { RegisterConfirmComponent } from './components/register-confirm/register-confirm.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { LegalNoticeComponent } from './components/legal-notice/legal-notice.component';
import { RGPDComponent } from './components/rgpd/rgpd.component';
import { ContactComponent } from './components/contact/contact.component';
import { NewsComponent } from './components/news/news.component';


@NgModule({
  declarations: [
    AppComponent,
    ConfirmDeleteAccountComponent,
    ContactComponent,
    ForgottenPasswordComponent,
    HomePageComponent,
    LegalNoticeComponent,
    LoginComponent,
    LogoutComponent,
    NewsComponent,
    NotificationComponent,
    RegisterComponent,
    RegisterConfirmComponent,
    ResetPasswordComponent,
    RGPDComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
  ],
  providers: [ 
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: CredentialsInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
