import Color from './color';

export default interface Activity {
	id: string
	label : string,
	title : string,
	type: string,
	color : Color,
}