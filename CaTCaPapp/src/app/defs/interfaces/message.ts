export default interface Message {
	content: string;
	uid: string;
}