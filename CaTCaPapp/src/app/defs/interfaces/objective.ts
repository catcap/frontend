import Color from './color';

export default interface Objective {
	id: string
	label : string,
	title : string,
	type: string,
	color : Color,
}