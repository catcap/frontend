import Color from "./color";

export default interface Performance {
	id: string,
	label: string,
	title: string,
	type: string,
	color: Color
}