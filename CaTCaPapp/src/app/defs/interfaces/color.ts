export default interface Color {
	background : string,
	highlight : Highlight,
}

interface Highlight {
	background : string,
}