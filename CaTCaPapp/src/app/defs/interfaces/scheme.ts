import Color from './color';

export default interface Scheme {
	id: string
	label : string,
	title : string,
	type: string,
	color : Color,
}