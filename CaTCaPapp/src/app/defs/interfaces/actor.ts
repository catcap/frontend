import Color from './color';

export default interface Actor {
	id: string,
	label : string,
	title: string,
	type: string,
	color : Color|null,
}
