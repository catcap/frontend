import Color from './color';

export default interface Situation {
	id: string
	label : string,
	title : string,
	type: string,
	color : Color,
}