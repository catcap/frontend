export default interface Edge {
	from : number,
	to : number,
	label : string,
	type: string
}