import Color from './color';

export default interface Action {
    id: string,
    label: string,
    title: string,
    type: string,
    color: Color,
}