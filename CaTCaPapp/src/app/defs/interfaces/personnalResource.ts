import Color from './color';

export default interface Personnal_Resource {
	id: string
	label : string,
	title : string,
	type: string,
	color : Color,
}