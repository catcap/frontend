export enum API {
	// PROXYMA routes
	Post_Data = 'data_handler/handle_data',
	Proxyma_List = 'maps/proxyma_list',
	Select_Proxyma = 'maps/select_proxyma',
	Delete_Proxyma = 'data_handler/handle_data',

	// Data routes
	GetCorpusNames = 'plug/getCorpusNames',
	GetData = 'plug/getCorpus',
	GetRawData = 'plug/rawData',
	GetPreMap = 'plug/preMap',
	PostRawData = 'plug/handle_scraping',
	PostData = 'plug/postData',
	ValidateActivity = 'plug/validateActivity',
	DeleteCorpus = 'plug/deleteCorpus',

	// Authentication routes
	Groups = 'auth/group',
	Register = 'auth/register',
	Confirm_register = 'auth/confirm_register',
	Login = 'auth/login',
	Logout = 'auth/logout',
	Token_refresh = 'auth/refresh',
	Token_verify = 'auth/verify',
	Forgotten_Password = 'auth/forgotten_password',
	Reset_Password = 'auth/reset_password',
	Change_Password = 'auth/change_password',
	Delete_Account = 'auth/delete_account',
	Confirm_Delete_Account = 'auth/confirm_delete_account',
	Me = 'auth/me',

	// Admin routes
	AdminUsers = 'admin/users',
	AdminGroups = 'admin/groups',
	AdminNotif = 'admin/notification',

	// Content routes
	GetContent = 'content/get_content',
	HandleContent = 'content/handle_content',

	// WebSocket routes
	Notifications = 'notifications',

	// Contact routes
	Contact = 'contact',
}