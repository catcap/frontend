import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';


import { RoutesNames } from './defs/enums/routes';
import { HomePageComponent } from './components/home-page/home-page.component';
import { RegisterComponent } from './components/register/register.component';
import { RegisterConfirmComponent } from './components/register-confirm/register-confirm.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ForgottenPasswordComponent } from './components/forgotten-password/forgotten-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ConfirmDeleteAccountComponent } from './components/confirm-delete-account/confirm-delete-account.component';
import { UpdateManifestComponent } from './components/update-manifest/update-manifest.component';
import { AddonComponent } from './components/addon/addon.component';
import { RGPDComponent } from './components/rgpd/rgpd.component';
import { LegalNoticeComponent } from './components/legal-notice/legal-notice.component';
import { ContactComponent } from './components/contact/contact.component';

const routes: Routes = [
	{ path: RoutesNames.HomePage, component: HomePageComponent, },
	{ path: RoutesNames.Register, component: RegisterComponent, },
	{ path: RoutesNames.Confirm_Register, component: RegisterConfirmComponent, },
	{ path: RoutesNames.Login, component: LoginComponent, },
	{ path: RoutesNames.Logout, component: LogoutComponent, canActivate: [AuthGuard], },
	{ path: RoutesNames.Forgotten_Password, component: ForgottenPasswordComponent },
	{ path: RoutesNames.Reset_Password, component: ResetPasswordComponent },
	{ path: RoutesNames.Confirm_Delete, component: ConfirmDeleteAccountComponent },
	{ path: RoutesNames.Carrousel, loadChildren: () => import('./modules/carousel/carousel.module').then(m => m.CarouselModule), },
	{ path: RoutesNames.Maps, loadChildren: () => import('./modules/maps/maps.module').then(m => m.MapsModule), canActivate: [AuthGuard], },
	{ path: RoutesNames.Account, loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule), canActivate: [AuthGuard], },
	{ path: RoutesNames.Data, loadChildren: () => import('./modules/data-scrap/data-scrap.module').then(m => m.DataScrapModule), canActivate: [AuthGuard], },
	{ path: RoutesNames.Update, component: UpdateManifestComponent, },
	{ path: RoutesNames.Addon, component: AddonComponent, },
	{ path: RoutesNames.RGPD, component: RGPDComponent },
	{ path: RoutesNames.LegalNotice, component: LegalNoticeComponent },
	{ path: RoutesNames.Contact, component: ContactComponent },
	{ path: '**', component: HomePageComponent, },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
