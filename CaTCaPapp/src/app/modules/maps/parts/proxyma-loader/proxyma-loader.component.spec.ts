import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxymaLoaderComponent } from './proxyma-loader.component';

describe('ProxymaLoaderComponent', () => {
  let component: ProxymaLoaderComponent;
  let fixture: ComponentFixture<ProxymaLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProxymaLoaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProxymaLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
