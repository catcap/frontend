import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProxymaService } from 'src/app/services/proxyma/proxyma.service';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { WebsocketService } from 'src/app/services/websocket/websocket.service';

@Component({
	selector: 'app-proxyma-loader',
	templateUrl: './proxyma-loader.component.html',
	styleUrls: ['./proxyma-loader.component.scss']
})
export class ProxymaLoaderComponent {
	faCheck = faCheck as IconProp;
	proxyma_list: string[] = [];

	dataSetForm = this.formBuilder.group({
		proxyma: [null, [Validators.required]]
	});

	@Output() loadSignal: EventEmitter<object> = new EventEmitter();

	constructor(
		private proxymaService: ProxymaService,
		private formBuilder: FormBuilder,
		private webSocketService: WebsocketService,
	) {
		this.getProxymaList();
		this.webSocketService.notificationEventEmitter.subscribe({
			next: () => this.getProxymaList()
		})
		this.proxymaService.reloadProxyma.subscribe({
			next: () => this.getProxymaList()
		})
	}

	getProxymaList() {
		this.proxymaService.getProxymaList().subscribe({
			next: (res: string[]) => {
				this.proxyma_list = [];
				this.proxyma_list = res;
			}
		})
	}

	onSubmit() {
		this.loadSignal.emit(this.dataSetForm.value.proxyma);
	}
}
