import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareMapsComponent } from './compare-maps.component';

describe('CompareMapsComponent', () => {
  let component: CompareMapsComponent;
  let fixture: ComponentFixture<CompareMapsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CompareMapsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CompareMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
