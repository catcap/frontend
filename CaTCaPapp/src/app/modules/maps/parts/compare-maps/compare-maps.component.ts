import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { ProxymaService } from 'src/app/services/proxyma/proxyma.service';

@Component({
  selector: 'app-compare-maps',
  templateUrl: './compare-maps.component.html',
  styleUrl: './compare-maps.component.scss'
})
export class CompareMapsComponent {
  faCheck = faCheck as IconProp;
  proxyma_list: string[] = [];
  dataSetForm: FormGroup;
  
  @Output() loadSignal: EventEmitter<object> = new EventEmitter();

  constructor(
    private proxymaService: ProxymaService,
  ) {
    this.dataSetForm = new FormGroup({
      proxyma: new FormControl(null, [Validators.required]),
    })

    this.proxymaService.getProxymaList().subscribe({
      next: (res: string[]) => {
        this.proxyma_list = [];
        this.proxyma_list = res;
      }
    });
  }

  onSubmit() {
    this.loadSignal.emit(this.dataSetForm.value.proxyma);
  }
}
