import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faAngleDown, faAngleUp, faArrowRotateBack, faCircleXmark } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-map-filters',
	templateUrl: './map-filters.component.html',
	styleUrls: ['./map-filters.component.scss']
})
export class MapFiltersComponent implements OnChanges {
	@Input() mapLoaded: boolean = false;

	faAngle = faAngleDown as IconProp;
	faArrowRotateBack = faArrowRotateBack as IconProp;
	faCircleXmark = faCircleXmark as IconProp;

	filterToggle: boolean = false;
	dateFilterGroup: FormGroup;
	labelFilterGroup: FormGroup;

	@Output() filters: EventEmitter<object> = new EventEmitter();

	constructor() {
		this.dateFilterGroup = new FormGroup({
			date: new FormControl(null, Validators.required)
		})
		this.labelFilterGroup = new FormGroup({
			all: new FormControl(true),
			Action: new FormControl(true),
			Activity: new FormControl(true),
			Actor: new FormControl(true),
			Environmental_Resource: new FormControl(true),
			Objective: new FormControl(true),
			Performance: new FormControl(true),
			Personnal_Resource: new FormControl(true),
			Scheme: new FormControl(true),
			Situation: new FormControl(true),
		})

		 this.labelFilterGroup.valueChanges.subscribe((newValues) => {
			const allChecked = Object.values(newValues).every((value) => value === true);
			this.labelFilterGroup.get('all').setValue(allChecked, { emitEvent: false });
		  });
		  
		  this.labelFilterGroup.get('all').valueChanges.subscribe((newAllValue) => {
			if (newAllValue) {
			  Object.keys(this.labelFilterGroup.controls).forEach((controlName) => {
				if (controlName !== 'all') {
				  this.labelFilterGroup.get(controlName).setValue(true, { emitEvent: false });
				}
			  });
			} else {
			  Object.keys(this.labelFilterGroup.controls).forEach((controlName) => {
				if (controlName !== 'all') {
				  this.labelFilterGroup.get(controlName).setValue(false, { emitEvent: false });
				}
			  });
			}
		  });
	  
		  Object.keys(this.labelFilterGroup.controls).forEach((controlName) => {
			if (controlName !== 'all') {
			  this.labelFilterGroup.get(controlName).valueChanges.subscribe(() => {
				const allOthersTrue = Object.keys(this.labelFilterGroup.controls)
				  .filter((key) => key !== 'all')
				  .every((key) => this.labelFilterGroup.get(key).value === true);
				if (allOthersTrue) {
				  this.labelFilterGroup.get('all').setValue(true, { emitEvent: false });
				}
			  });
			}
		  });
	}

	toggleFilters(): void {
		this.faAngle = this.filterToggle ? faAngleDown as IconProp : faAngleUp as IconProp;
		this.filterToggle = !this.filterToggle;
	}

	get date() {
		return this.dateFilterGroup.get("date");
	}

	filter(): void {
		const labels: string[] = [];
		Object.keys(this.labelFilterGroup.controls).forEach(formControl => {
			if (formControl !== "all" && this.labelFilterGroup.get(formControl).value) labels.push(formControl);
		});
		const filters = {
			time: this.date.value,
			labels: labels,
		}
		this.filters.emit(filters);
	}

	removeFilter(): void {
		this.dateFilterGroup.patchValue({date: null});
		this.labelFilterGroup.patchValue({all: true});
		this.filters.emit({zero: true});
	}


	ngOnChanges(changes: SimpleChanges): void {		
		if (changes["mapLoaded"]) {
			const newValue = changes["mapLoaded"].currentValue;
			if (!newValue) {
				this.dateFilterGroup.patchValue({date: null});
				this.labelFilterGroup.patchValue({all: true});
				this.filterToggle = false;
			}
		}
	}
}
