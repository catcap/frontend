import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import * as FileSaver from 'file-saver';
import { FileUploadService } from 'src/app/services/file_upload/file-upload.service';
import { v4 as uuidv4 } from 'uuid';
import { faSave, faArrowRotateLeft, faCheck, faTrash, faDownload, faUpload, faMagnifyingGlass, faPlus, faCirclePlus, faLink, faSquarePen, faLinkSlash, faFolderOpen } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { ProxymaService } from 'src/app/services/proxyma/proxyma.service';

@Component({
	selector: 'app-modify-map',
	templateUrl: './modify-map.component.html',
	styleUrls: ['./modify-map.component.scss']
})
export class ModifyMapComponent implements OnInit{
	@Input() data: object[];
	@Input() edgeNumber: number = 0;
	@Input() type: string | undefined;

	projectTitle: string;
	createNodes: boolean = false;
	relyNodes: boolean = false;
	detachNodes: boolean = false;
	editNodes: boolean = false;
	uploadMap: boolean = false;
	downloadMap: boolean = false;
	loadMap: boolean = false;
	showFields: number = 0;
	selectedFile: File | null = null;
	downloadTitle: string;
	subscription: Subscription;
	nodeForm: FormGroup;
	edgeForm: FormGroup;
	lastUnsavedChanges: boolean = false;
	alertMessage: boolean = false;
	saving: boolean = false;

	faArrowRotateLeft = faArrowRotateLeft as IconProp;
	faCheck = faCheck as IconProp;
	faCirclePlus = faCirclePlus as IconProp;
	faDownload = faDownload as IconProp;
	faFolderOpen = faFolderOpen as IconProp;
	faMagnifyingGlass = faMagnifyingGlass as IconProp;
	faLink = faLink as IconProp
	faLinkSlash = faLinkSlash as IconProp;
	faPlus = faPlus as IconProp;
	faSave = faSave as IconProp;
	faSquarePen = faSquarePen as IconProp;
	faTrash = faTrash as IconProp;
	faUpload = faUpload as IconProp;

	@Output() unsavedChanges: EventEmitter<boolean> = new EventEmitter();
	@Output() newNode: EventEmitter<object> = new EventEmitter();
	@Output() newEdge: EventEmitter<object> = new EventEmitter();
	@Output() modifyNode: EventEmitter<object> = new EventEmitter();
	@Output() destroyNode: EventEmitter<string> = new EventEmitter();
	@Output() detachNode: EventEmitter<object> = new EventEmitter();
	@Output() rollback: EventEmitter<void> = new EventEmitter();

	constructor(
		private fileUploadService: FileUploadService,
		private proxymaService: ProxymaService,
		private formBuilder: FormBuilder,
	) {
		this.unsavedChanges.subscribe((value: boolean) => this.lastUnsavedChanges = value);

		this.nodeForm = new FormGroup({
			nodeType: new FormControl(),
			node: new FormControl(),
		})
		
		this.edgeForm = new FormGroup({
			from: new FormControl(null, [Validators.required]),
			to: new FormControl(null, [Validators.required]),
		})
	}
	
	ngOnInit(): void {
		for (let elem of this.data) {
			if (elem['labels'].includes('Proxyma_Corpus')) {
				this.projectTitle = elem['properties']['title']
			}
		}
	}

	toggleCreateNode(state: boolean): void {
		this.createNodes = state;
		if (state) {
			this.subscription = this.nodeForm.get('nodeType').valueChanges.subscribe((value: string) => {
				this.watchNodeTypes(this.nodeForm, value);
			})
		} else {
			this.subscription.unsubscribe();
		}
	}

	toggleRelyNode(state: boolean): void {
		this.relyNodes = state;
		if (!state) {
			this.edgeForm.reset();
		}
	}

	toggleDetachNode(state: boolean): void {
		this.detachNodes = state;
		if (!state) {
			this.edgeForm.reset();
		}
	}

	toggleEditNode(state: boolean): void {
		this.editNodes = state;
		if (state) {
			this.subscription = this.nodeForm.get('node').valueChanges.subscribe((value: string) => {
				this.watchNodes(this.nodeForm, value);
			})
		} else {
			this.subscription.unsubscribe();
		}
	}

	watchNodeTypes(formGroup: FormGroup, formControlValue: string): void {
		Object.keys(formGroup.controls).forEach((key: string) => {
			if (key !== 'nodeType' && key !== 'node') formGroup.removeControl(key);
		});
		switch (formControlValue) {
			case 'Action':
			case 'Activity':
			case 'Objective':
			case 'Situation':
			case 'Environmental_Resource':
			case 'Personnal_Resource':
				this.nodeForm.addControl('description', this.formBuilder.control('', [Validators.required, Validators.maxLength(500)]));
				this.showFields = 1;
				if (['Environmental_Resource', 'Personnal_Resource'].includes(formControlValue)) {
					this.nodeForm.addControl('description', this.formBuilder.control('', [Validators.required, Validators.maxLength(500)]));
					this.nodeForm.addControl('type', this.formBuilder.control('', [Validators.required]));
				}
				this.showFields = 1;
				break;
			case 'Actor':
				this.nodeForm.addControl('name', this.formBuilder.control('', [Validators.required, Validators.maxLength(500)]));
				this.showFields = 2;
				break;
			case 'Performance':
				this.nodeForm.addControl('criterion', this.formBuilder.control('', [Validators.required, Validators.maxLength(50)]));
				this.nodeForm.addControl('value', this.formBuilder.control('', [Validators.required]));
				this.nodeForm.addControl('unit', this.formBuilder.control('', [Validators.required, Validators.maxLength(10)]))
				this.showFields = 3;
				break;
			case 'Scheme':
				this.nodeForm.addControl('actions_sequence', this.formBuilder.control('', [Validators.required]));
				this.showFields = 4;
				break;
			default:
				break;
		}
	}

	watchNodes(formGroup: FormGroup, formControlValue: string): void {
		Object.keys(formGroup.controls).forEach((key: string) => {
			if (key !== 'nodeType' && key !== 'node') formGroup.removeControl(key);
		})
		const nodeType = formControlValue['labels'][0];
		const properties = formControlValue['properties'];
		this.nodeForm.get('nodeType').setValue(nodeType, {emitEvent: false});
		switch (nodeType) {
			case 'Action':
			case 'Activity':
			case 'Objective':
			case 'Situation':
			case 'Environmental_Resource':
			case 'Personnal_Resource':
				this.nodeForm.addControl('description', this.formBuilder.control(properties['description'], [Validators.required, Validators.maxLength(500)]));
				if (['Environmental_Resource', 'Personnal_Resource'].includes(nodeType)) {
					this.nodeForm.addControl('type', this.formBuilder.control(properties['type'], [Validators.required]));
				}
				this.showFields = 1;
				break;
			case 'Actor':
				this.nodeForm.addControl('name', this.formBuilder.control(properties['name'], [Validators.required, Validators.maxLength(500)]));
				this.showFields = 2;
				break;
			case 'Performance':
				this.nodeForm.addControl('criterion', this.formBuilder.control(properties['criterion'], [Validators.required, Validators.maxLength(50)]));
				this.nodeForm.addControl('value', this.formBuilder.control(properties['value'], [Validators.required]));
				this.nodeForm.addControl('unit', this.formBuilder.control(properties['unit'], [Validators.required, Validators.maxLength(10)]))
				this.showFields = 3;
				break;
			case 'Scheme':
				this.nodeForm.addControl('actions_sequence', this.formBuilder.control(properties['actions_sequence'], [Validators.required]));
				this.showFields = 4;
				break;
			default:
				break;
		}
	}


	addNode(): void {
		const type = this.nodeForm.get('nodeType').value;
		const properties = {};
		this.unsavedChanges.emit(true);
		Object.keys(this.nodeForm.controls).forEach((key: string) => {
			if (key !== 'nodeType') {
				properties[key] = this.nodeForm.get(key).value; 
			}
		})
		
		const jsonObject = {
			'labels': [type],
			'properties': {
				'uid': uuidv4(),
			},
			'relationships': {}
		};

		let label: string;
		let background: string;
		let highlight: string;

		for (let property in properties) {
			if (property !== 'node' && property !== 'nodeType') jsonObject.properties[property] = properties[property];
		}

		this.data.push(jsonObject);
		switch (type) {
			case 'Action':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['actions'].push(jsonObject.properties.uid);
				}
				label = properties['description'];
				background = '#AAAAAA';
				highlight = '#FFFFFF';
				break;
			case 'Activity':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['activities'].push(jsonObject.properties.uid);
				}
				jsonObject.relationships = {
					objectives: [],
					schemes: [],
					situations: [],
				}
				label = properties['description'];
				background = '#549CDD';
				highlight = '#0087FF';
				break;
			case 'Actor':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['actors'].push(jsonObject.properties.uid);
				}
				jsonObject.relationships = {
					personnal_resources: [],
				}
				label = properties['name'];
				background = '#FABC1C';
				highlight = '#FFB700';
				break;
			case 'Environmental_Resource':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['environmental_resources'].push(jsonObject.properties.uid);
				}
				label = properties['description'];
				background = '#6EC667';
				highlight = '#11FF00';
				break;
			case 'Objective':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['objectives'].push(jsonObject.properties.uid);
				}
				jsonObject.relationships = {
					performances: [],
				}
				label = properties['description'];
				background = '#E95615';
				highlight = '#FF5000';
				break;
			case 'Performance':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['performances'].push(jsonObject.properties.uid);
				}
				label = `${properties['criterion']} : ${properties['value']}${properties['unit']}`;
				background = '#F4A09F';
				highlight = '#FF0200';
				break;
			case 'Personnal_Resource':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['personnal_resources'].push(jsonObject.properties.uid);
				}
				label = properties['description'];
				background = '#F79767';
				highlight = '#FF97AA';
				break;
			case 'Proxyma_Corpus':
				jsonObject.relationships = {
					actions: [],
					activities: [],
					actors: [],
					environmental_resources: [],
					objectives: [],
					performances: [],
					personnal_resources: [],
					schemes: [],
					situations: [],
				}
				this.projectTitle = jsonObject.properties['title'];
				break;
			case 'Scheme':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['schemes'].push(jsonObject.properties.uid);
				}
				jsonObject.relationships = {
					actions: [],
					actors: [],
					environmental_resources: [],
				}
				label = properties['actions_sequence'];
				background = '#00CCFF';
				highlight = '#57C7E3';
				break;
			case 'Situation':
				for (let node of this.data) {
					if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['situations'].push(jsonObject.properties.uid);
				}
				label = properties['description'];
				background = '#D9D9D9';
				highlight = '#F0F0F0';
				break;
			default:
				break;
		}

		const node = {
			id: jsonObject.properties['uid'],
			label: label,
			color: {
				background: background,
				highlight: {
					background: highlight,
				}
			}
		}
		if (type !== 'Proxyma_Corpus') {
			this.newNode.emit(node);
		};
		this.showFields = 0;
		this.nodeForm.reset();
	}


	updateNode(): void {
		this.unsavedChanges.emit(true);
		const type = this.nodeForm.get('nodeType').value;
		const uid = this.nodeForm.get('node').value['properties']['uid'];
		const node = { id: uid, label: '' }
		Object.keys(this.nodeForm.controls).forEach((key: string) => {
			if (!['node', 'nodeType'].includes(key)) {
				this.data.forEach(elem => {
					if (elem['properties']['uid'] === uid && elem['labels'][0] === type) {
						elem['properties'][key] = this.nodeForm.get(key).value;
						node['label'] = elem['properties']['description'] || elem['properties']['name'] || elem['properties']['actions_sequence'] || elem['properties']['criterion'] + ' ' + elem['properties']['value'] + elem['properties']['unit'];
						
					}
				})
			}
		})
		this.modifyNode.emit(node);
	}

	deleteNode(nodeId: string) : void {
		this.unsavedChanges.emit(true);
		this.data.forEach((elem, index) => {
			if (elem['properties']['uid'] === nodeId) {
				this.data.splice(index, 1);
				this.destroyNode.emit(nodeId);
			}
			for (let el in elem['relationships']) {
				if (elem['relationships'][el].includes(nodeId)) {
					elem['relationships'][el].splice(elem['relationships'][el].findIndex((e) => e == nodeId), 1);
				}
			}
		})
		this.goBack();
	}


	getFromPermission(nodeType: string): boolean {
		if (['Activity', 'Actor', 'Objective', 'Scheme'].includes(nodeType)) return true;
		else return false;
	}

	getToPermission(nodeType: string): object[] {
		const authorizedNodes: object[] = [];
		for (let node of this.data) {
			switch (nodeType) {
				case 'Activity':
					if (['Objective', 'Scheme', 'Situation'].includes(node['labels'][0])) authorizedNodes.push(node);
					break;
				case 'Actor':
					if (['Personnal_Resource'].includes(node['labels'][0])) authorizedNodes.push(node);
					break;
				case 'Objective':
					if (['Performance'].includes(node['labels'][0])) authorizedNodes.push(node);
					break;
				case 'Scheme':
					if (['Action', 'Actor', 'Environmental_Resource'].includes(node['labels'][0])) authorizedNodes.push(node);
					break;
				default:
					break;
			}
		}
		return authorizedNodes;
	}

	isConnected(from: Object, to: Object): boolean {
		return Object.values(from['relationships']).some((arr: string[]) => arr.includes(to['properties']['uid']));
	}

	disconnectNodes(): void {
		this.unsavedChanges.emit(true);
		const node1 = this.edgeForm.get('from').value;
		const node2 = this.edgeForm.get('to').value;
		for (let node of this.data) {
			if (node['properties']['uid'] === node1['properties']['uid']) {
				switch (node2['labels'][0]) {
					case 'Action':
						node['relationships']['actions'].splice(node['relationships']['actions'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					case 'Actor':
						node['relationships']['actors'].splice(node['relationships']['actors'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					case 'Environmental_Resource':
						node['relationships']['environmental_resources'].splice(node['relationships']['environmental_resources'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					case 'Objective':
						node['relationships']['objectives'].splice(node['relationships']['objectives'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					case 'Performance':
						node['relationships']['performances'].splice(node['relationships']['performances'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					case 'Personnal_Resource':
						node['relationships']['personnal_resources'].splice(node['relationships']['personnal_resources'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					case 'Scheme':
						node['relationships']['schemes'].splice(node['relationships']['schemes'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					case 'Situation':
						node['relationships']['situations'].splice(node['relationships']['situations'].findIndex((elem) => elem === node2['properties']['uid']), 1);
						break;
					default:
						break;
				}
			}
		}

		const edge = {
			from: node1['properties']['uid'],
			to: node2['properties']['uid'],
		}
		this.detachNode.emit(edge);
		this.edgeNumber --;
		this.edgeForm.get('to').reset();
	}

	connectNodes(): void {
		this.unsavedChanges.emit(true);
		const node1 = this.edgeForm.get('from').value;
		const node2 = this.edgeForm.get('to').value;
		for (let node of this.data) {
			if (node['properties']['uid'] === node1['properties']['uid']) {
				switch (node2['labels'][0]) {
					case 'Action':
						node['relationships']['actions'].push(node2['properties']['uid']);
						break;
					case 'Actor':
						node['relationships']['actors'].push(node2['properties']['uid']);
						break;
					case 'Environmental_Resource':
						node['relationships']['environmental_resources'].push(node2['properties']['uid']);
						break;
					case 'Objective':
						node['relationships']['objectives'].push(node2['properties']['uid']);
						break;
					case 'Performance':
						node['relationships']['performances'].push(node2['properties']['uid']);
						break;
					case 'Personnal_Resource':
						node['relationships']['personnal_resources'].push(node2['properties']['uid']);
						break;
					case 'Scheme':
						node['relationships']['schemes'].push(node2['properties']['uid']);
						break;
					case 'Situation':
						node['relationships']['situations'].push(node2['properties']['uid']);
						break;
					default:
						break;
				}
			}
		}

		const edge = {
			from: node1['properties']['uid'],
			to: node2['properties']['uid'],
		}
		this.newEdge.emit(edge);
		this.edgeNumber ++;
		this.edgeForm.get('to').reset();
	}

	onFileSelected(event: Event): void {
		const input = event.target as HTMLInputElement;
		if (input.files && input.files.length > 0) {
			this.selectedFile = input.files[0];
		}
	}

	mapComparator(corpus: object): void {
		this.proxymaService.proxymaLoader(corpus['id']).subscribe({
			next: (res) => {
				for (let elem of res['data']) {
					if (!elem['labels'].includes('Proxyma_Corpus')) {
						this.data.forEach(node => {
							if (node['labels'].includes('Proxyma_Corpus')) {
								switch(elem['labels'][0]) {
									case 'Action':
										node['relationships']['actions'].push(elem['properties']['uid']);
										break;
									case 'Activity':
										node['relationships']['activities'].push(elem['properties']['uid']);
										break;
									case 'Actor':
										node['relationships']['actors'].push(elem['properties']['uid']);
										break;
									case 'Environmental_Resource':
										node['relationships']['environmental_resources'].push(elem['properties']['uid']);
										break;
									case 'Objective':
										node['relationships']['objectives'].push(elem['properties']['uid']);
										break;
									case 'Performance':
										node['relationships']['performances'].push(elem['properties']['uid']);
										break;
									case 'Personnal_Resource':
										node['relationships']['personnal_resources'].push(elem['properties']['uid']);
										break;
									case 'Scheme':
										node['relationships']['schemes'].push(elem['properties']['uid']);
										break;
									case 'Situation':
										node['relationships']['situations'].push(elem['properties']['uid']);
										break;
									default:
										break;
								}
							}
						});
						this.data.push(elem)
						this.newNode.emit({
							id: elem['properties']['uid'],
							label: elem['properties']['description'] || elem['properties']['name'] || elem['properties']['actions_sequence'] || elem['properties']['criterion'] + ' : ' + elem['properties']['value'] + ' ' + elem['properties']['unit'],
							type: elem['labels'][0],
							begin: elem['properties']['created_at'],
							end: elem['properties']['deleted_at'],
							color: {
								background: elem['properties']['background'],
								highlight: {
									background: elem['properties']['highlight'],
								}
							}
						})
						for (let part in elem['relationships']) {
							for (let uid of elem['relationships'][part]) {
								this.newEdge.emit({
									from: elem['properties']['uid'],
									to: uid,
								})
								this.edgeNumber++;
							}
						}
					}
				}
			}
		})
	}

	upload(): void {
		if (this.selectedFile) {
			this.unsavedChanges.emit(true);
			const reader = new FileReader();
			reader.onload = (e) => {
				const fileContent = e.target?.result as string;
				const parsedData = JSON.parse(fileContent) as object[];
				try {
					for (let data of parsedData) {
						const oldUID = data['properties']['uid'];
						const newUID = uuidv4();
						data['properties']['uid'] = newUID;
						for (let elem of parsedData) {
							for (let part in elem['relationships']) {
								const indexUID = elem['relationships'][part].indexOf(oldUID);
								if (indexUID !== -1) {
									elem['relationships'][part][indexUID] = newUID;
								}
							}
						}
					}
					for (let data of parsedData) {
						if (!data['labels'].includes('Proxyma_Corpus')) {
							this.data.push(data);
							let background: string;
							let highlight: string;
							switch (data['labels'][0]) {
								case 'Action':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['actions'].push(data['properties']['uid']);
									}
									background = '#AAAAAA';
									highlight = '#FFFFFF';
									break;
								case 'Activity':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['activities'].push(data['properties']['uid']);
									}
									background = '#549CDD';
									highlight = '#0087FF';
									break;
								case 'Actor':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['actors'].push(data['properties']['uid']);
									}
									background = '#FABC1C';
									highlight = '#FFB700';
									break;
								case 'Environmental_Resource':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['environmental_resources'].push(data['properties']['uid']);
									}
									background = '#6EC667';
									highlight = '#11FF00';
									break;
								case 'Objective':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['objectives'].push(data['properties']['uid']);
									}
									background = '#E95615';
									highlight = '#FF5000';
									break;
								case 'Performance':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['performances'].push(data['properties']['uid']);
									}
									background = '#F4A09F';
									highlight = '#FF0200';
									break;
								case 'Personnal_Resource':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['personnal_resources'].push(data['properties']['uid']);
									}
									background = '#F79767';
									highlight = '#FF97AA';
									break;
								case 'Scheme':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['schemes'].push(data['properties']['uid']);
									}
									background = '#00CCFF';
									highlight = '#57C7E3';
									break;
								case 'Situation':
									for (let node of this.data) {
										if (node['labels'].includes('Proxyma_Corpus')) node['relationships']['situations'].push(data['properties']['uid']);
									}
									background = '#D9D9D9';
									highlight = '#F0F0F0';
									break;
							}
							const node = {
								id: data['properties']['uid'],
								label: data['properties']['description'] || data['properties']['name'] || data['properties']['actions_sequence'] || data['properties']['criterion'] + ' : ' + data['properties']['value'] + data['properties']['unit'],
								color: {
									background: background,
									highlight: {
										background: highlight,
									}
								}
							}
							this.newNode.emit(node);

							const relationships = data['relationships'];
							for (let part in relationships) {
								for (let uid of relationships[part]) {
									const edge = {
										from: data['properties']['uid'],
										to: uid,
									}
									this.newEdge.emit(edge);
								}
							}
						}
					}
				} catch (error) {
					console.error(`Erreur lors de l'analyse du fichier JSON : ${error}`)
				}
			};
			reader.readAsText(this.selectedFile);
		}
	}

	onDownloadRenamed(event: Event): void {
		const input = event.target as HTMLInputElement;
		if (input.value) {
			this.downloadTitle = input.value;
		}
	}

	download(): void {
		const data = this.data;
		this.downloadTitle = this.downloadTitle ? this.downloadTitle : this.projectTitle;
		data.forEach(elem => {
			if (elem['labels'].includes('Proxyma_Corpus')) {
				elem['properties']['title'] = this.downloadTitle;
			}
		})
		const jsonString = JSON.stringify(data, null, 2);
		const blob = new Blob([jsonString], { type: 'application/json' });
		FileSaver.saveAs(blob, `${this.downloadTitle}.json`)
	}

	saveMap(): void {
		const proxyma_id = this.data.find(object => object['labels'][0] === 'Proxyma_Corpus')['properties']['uid'];
		this.fileUploadService.updateProxyma(proxyma_id, this.data).subscribe({
			complete: () => {
				this.unsavedChanges.emit(false);
				this.saving = true;
			},
		});
	}

	goBack(force: boolean = false) {
		if (force) {
			this.data.forEach((elem) => {
				this.destroyNode.emit(elem['properties']['uid']);
			})
			this.data = [];
			this.unsavedChanges.emit(false);
			this.rollback.emit();
		}
		if ((!this.createNodes && !this.relyNodes && !this.detachNodes && !this.editNodes && !this.uploadMap && !this.loadMap)) {
			if (this.lastUnsavedChanges) {
				this.alertMessage = true;
			} else {
				this.data.forEach((elem) => {
					this.destroyNode.emit(elem['properties']['uid']);
				})
				this.data = [];
				this.unsavedChanges.emit(false);
				this.rollback.emit();
			}
		};
		if (this.createNodes) this.toggleCreateNode(false);
		if (this.relyNodes) this.toggleRelyNode(false);
		if (this.detachNodes) this.toggleDetachNode(false);
		if (this.editNodes) this.toggleEditNode(false);
		if (this.uploadMap) this.uploadMap = false;
		if (this.loadMap) this.loadMap = false;
	}
}
