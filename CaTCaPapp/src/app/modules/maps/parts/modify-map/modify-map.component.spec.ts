import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyMapComponent } from './modify-map.component';

describe('ModifyMapComponent', () => {
  let component: ModifyMapComponent;
  let fixture: ComponentFixture<ModifyMapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModifyMapComponent]
    });
    fixture = TestBed.createComponent(ModifyMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
