import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateMapComponent } from './parts/create-map/create-map.component';
import { MapFiltersComponent } from './parts/map-filters/map-filters.component';
import { ModifyMapComponent } from './parts/modify-map/modify-map.component';
import { ProxymaLoaderComponent } from './parts/proxyma-loader/proxyma-loader.component';
import { MapsComponent } from './maps.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ProxymaDeleterModule } from '../shared/proxyma-deleter/proxyma-deleter.module';
import { GroupSelectorModule } from '../shared/group-selector/group-selector.module';
import { AuthGuard } from 'src/app/guard/auth.guard';
import { LegendModule } from '../shared/legend/legend.module';
import { CompareMapsComponent } from './parts/compare-maps/compare-maps.component';



@NgModule({
  declarations: [
    MapsComponent,
    CreateMapComponent,
    ProxymaLoaderComponent,
    MapFiltersComponent,
    ModifyMapComponent,
    CompareMapsComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    ProxymaDeleterModule,
    GroupSelectorModule,
    LegendModule,
    RouterModule.forChild([{path: '', component: MapsComponent, canDeactivate: [AuthGuard]}])
  ],
  exports: [
    MapsComponent
  ],
  
})
export class MapsModule { }
