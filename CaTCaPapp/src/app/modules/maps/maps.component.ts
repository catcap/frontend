import { Component, ViewChild, AfterViewInit, OnInit, OnDestroy } from '@angular/core';
import { Network } from 'vis-network';
import { DataSet } from 'vis-data';

import { ProxymaService } from '../../services/proxyma/proxyma.service';

import { faArrowRotateBack, faEdit, faFileCirclePlus, faFilter, faPlus, faTrash } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { BreakpointsService } from '../../services/breakpoints/breakpoints.service';
import { Subscription, filter } from 'rxjs';
import { AlertService } from '../../services/alert/alert.service';

@Component({
	selector: 'app-maps',
	templateUrl: './maps.component.html',
	styleUrls: ['./maps.component.scss'],
})
export class MapsComponent implements OnInit, AfterViewInit, OnDestroy {
	faArrowRotateBack = faArrowRotateBack as IconProp;
	faEdit = faEdit as IconProp;
	faFileCirclePlus = faFileCirclePlus as IconProp;
	faFilter = faFilter as IconProp;
	faPlus = faPlus as IconProp;
	faTrash = faTrash as IconProp;

	alertMessage: boolean;
	nodes = new DataSet([]);
	edges = new DataSet([]);
	isLoading: boolean = false;
	status: string = null;
	alertSubscription: Subscription | undefined;
	breakpointsSubscription: Subscription | undefined;
	type: string | null = null;
	proxyma_title: string;
	proxyma_corpus_id: string;
	rawMap: object[] = [];
	saveNodes: string[] = [];
	saveEdges: string[] = [];
	unsavedChanges: boolean = false;

	network: Network;

	options: object = {
		autoResize: true,
		locale: 'fr',
		interaction: {
			multiselect: false,
		},
		manipulation: {
			enabled: false,
			initiallyActive: false,
		},
		physics: {
			forceAtlas2Based: {
				gravitationalConstant: -50,
				centralGravity: 0.01,
				springLength: 100,
				damping: 0.4,
				avoidOverlap: 1,
			},
			minVelocity: 0.75,
			solver: 'forceAtlas2Based',
		},
		nodes: {
			chosen: true,
			mass: 2,
			color: {
				border: '#000000',
				highlight: {
					border: '#40E0D0',
				},
			},
			font: {
				size: 14,
				color: '#000000',
			},
			shadow: {
				color: '#00000080',
				size: 10,
				x: 5,
				y: 5,
			},
			shape: 'circle',
			widthConstraint: {
				minimum: 100,
				maximum: 150,
			},
		},
		edges: {
			chosen: true,
			color: {
				highlight: '#40E0D0',
			},
			arrows: {
				to: {
					enabled: false,
					scaleFactor: 0.5,
				},
			},
			smooth: false,
			selectionWidth: (width: number) => {
				return width * 5;
			},
			width: 1,
			length: 150
		},
	};

	@ViewChild('networkContainer') networkContainer: any;

	constructor(
		private alertService: AlertService,
		private breakpointService: BreakpointsService,
		private proxymaService: ProxymaService,
	) { }

	load(corpus: object) {
		if (corpus) {
			this.isLoading = true;
			this.proxyma_corpus_id = corpus["id"];
			this.proxyma_title = corpus["title"];
			this.clearNetwork();
			this.loadProxyma(this.proxyma_corpus_id);
		}
	}

	setStatus(status: string = null, event: boolean = false): void {
		if (this.status === 'editMap' || (this.status === 'deleteMap' && !event)) {
			this.clearNetwork();
			this.rawMap = [];
			this.loadProxyma(this.proxyma_corpus_id);
		}
		if (["editMap", "deleteMap"].includes(status)) this.filter({zero: true});
		if (status === null || event) this.clearNetwork();
		this.status = status;
	}

	addNewNode(node): void {
		this.nodes.add(node);
	}

	updateNode(node): void {
		this.nodes.update(node);
	}

	destroyNode(nodeId): void {
		this.nodes.remove(nodeId);
	}

	addNewEdge(edge): void {
		this.edges.add(edge);
	}

	detachNode(edge): void {
		this.edges.remove(this.edges.get().find(elem => elem.from === edge["from"] && elem.to === edge["to"]));
	}

	setUnsavedChanges(status: boolean) {
		this.unsavedChanges = status;
	}

	hasUnsavedChanges(): boolean {
		return this.unsavedChanges;
	}
	

	private loadProxyma(corpus_uid: string) {
		this.proxymaService.proxymaLoader(corpus_uid).subscribe({
			next: (res) => {
				for (let elem of res["data"]) {
					this.rawMap.push(elem);
					if (!elem["labels"].includes("Proxyma_Corpus")) {
						this.nodes.add({
							id: elem["properties"]["uid"],
							label: elem["properties"]["description"] || elem["properties"]["name"] || elem["properties"]["actions_sequence"] || elem["properties"]["criterion"] + ' : ' + elem["properties"]["value"] + " " + elem["properties"]["unit"],
							type: elem["labels"][0],
							begin: elem["properties"]["created_at"],
							end: elem["properties"]["deleted_at"],
							color: {
								background: elem["properties"]["background"],
								highlight: {
									background: elem["properties"]["highlight"],
								}
							}
						});
						const relationships = elem["relationships"];
						for (let part in relationships) {
							for (let uid of relationships[part]) {
								this.edges.add({
									from: elem["properties"]["uid"],
									to: uid,
								});
							}
						}
					}
				}
				this.eraseVisData();
				this.saveVisData();
			},
			complete: () => {
				this.isLoading = false;
				this.status = "mapLoaded";
			}
		});
	}

	filter(filters): void {
		this.nodes.clear();
		if (filters["zero"]) this.loadVisData().nodes.forEach(node => this.nodes.add(node));
		else {
			this.loadVisData().nodes.forEach(node => this.nodes.add(node));
			const filteredNodes = this.nodes.get({
				filter: function (node) {
					return (
						(filters["time"]
							? filters["time"] >= node["begin"] && (node["end"] ? filters["time"] < node["end"] : true)
							: true
						) && filters["labels"].includes(node["type"])
					)
				}
			})
			this.nodes.clear();
			filteredNodes.forEach(node => this.nodes.add(node));
		}
		
	}

	private saveVisData() {
		this.nodes.forEach(node => this.saveNodes.push(node));
		this.edges.forEach(edge => this.saveEdges.push(edge));
	}

	private loadVisData() {
		return {
			nodes: this.saveNodes,
			edges: this.saveEdges
		};
	}

	private eraseVisData() {
		this.saveNodes = [];
		this.saveEdges = [];
	}

	private clearNetwork() {
		this.nodes.clear();
		this.edges.clear();
	}
	
	onContinue() {
		this.alertService.getUserResponse(true);
	}

	onCancel() {
		this.alertService.getUserResponse(false);
		this.alertMessage = false;
	}

	ngOnInit(): void {
		this.breakpointsSubscription = this.breakpointService.getTypeObservable().subscribe((type) => this.type = type);
		this.alertSubscription = this.alertService.showAlert$.subscribe(message => this.alertMessage = message);
	}

	ngAfterViewInit(): void {
		this.network = new Network(
			this.networkContainer.nativeElement,
			{ nodes: this.nodes, edges: this.edges },
			this.options
		);
	}

	ngOnDestroy(): void {
		this.breakpointsSubscription.unsubscribe();
		this.alertSubscription.unsubscribe();
	}
}
