import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataComponent } from './data.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { GroupSelectorModule } from '../shared/group-selector/group-selector.module';
import { RouterModule } from '@angular/router';
import { CorpusLoaderComponent } from './parts/corpus-loader/corpus-loader.component';
import { DataViewerComponent } from './parts/data-viewer/data-viewer.component';
import { DataVizComponent } from './parts/data-viz/data-viz.component';
import { PreMapComponent } from './parts/pre-map/pre-map.component';
import { LegendModule } from '../shared/legend/legend.module';
import { PreMapEditComponent } from './parts/pre-map-edit/pre-map-edit.component';


@NgModule({
  declarations: [
    DataComponent,
    CorpusLoaderComponent,
    DataViewerComponent,
    DataVizComponent,
    PreMapComponent,
    PreMapEditComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    GroupSelectorModule,
    LegendModule,
    RouterModule.forChild([{path: '', component: DataComponent}])
  ],
  exports: [
    DataComponent
  ]
})
export class DataScrapModule { }
