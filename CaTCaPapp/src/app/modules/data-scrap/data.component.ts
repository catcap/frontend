import { Component } from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent {
  corpus: Object;
  corpusId: string;
  corpusName: string;
  selectedFile: File | null = null;

  faCheck = faCheck as IconProp;  
  
  constructor(
    private dataService: DataService,
  ) { }

  load(corpus: object) {
    this.dataService.getData(corpus['id']).subscribe({
      next: (res) => {
        this.corpus = res['data'];
        this.corpusId = res['id'];
        this.corpusName = res['corpus'];
      },
    })
  }

  onFileSelected(event: any): void {
    if (event.target.files[0].type === "application/json") {
      this.selectedFile = event.target.files[0];
    }
  }

  clear(event: boolean): void {
    this.corpus = null;
    this.corpusId = null;
    this.corpusName = null;
    this.dataService.reload();
  }

  submitData(): void {
    const formData = new FormData();
    formData.append('data', this.selectedFile);
    this.dataService.postRawData(formData).subscribe();
  }
}
