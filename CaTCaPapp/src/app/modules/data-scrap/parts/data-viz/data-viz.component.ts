import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faArrowLeft, faArrowRight, faArrowRotateBack, faCheck, faMinus, faPlus, faX } from '@fortawesome/free-solid-svg-icons';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-data-viz',
  templateUrl: './data-viz.component.html',
  styleUrls: ['./data-viz.component.scss']
})
export class DataVizComponent implements OnChanges {
  @Input() selectData: object;
  @Input() corpusId: string;
  @Input() corpusName: string;

  faArrowLeft = faArrowLeft as IconProp;
  faArrowRight = faArrowRight as IconProp;
  faArrowRotateBack = faArrowRotateBack as IconProp;
  faCheck = faCheck as IconProp;
  faMinus = faMinus as IconProp;
  faPlus = faPlus as IconProp;
  faX = faX as IconProp;

  activityIndex: string;
  cleanFormGroup: FormGroup;
  corpusForm: FormGroup;
  openAction: boolean = false;
  preMap: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
  ) {
    this.dataService.refreshData.subscribe({
      next: () => {
        this.openAction = false;
        this.preMap = false;
      }
    })
    this.corpusForm = new FormGroup({
      date: new FormControl(''),
      activity: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(150)]),
      action: new FormControl('', [Validators.required]),
      actions: new FormArray([], [Validators.required]),
      actors: new FormArray([], [Validators.required]),
      objectives: new FormArray([], [Validators.required]),
      performances: new FormArray([], [Validators.required]),
      environmental_resources: new FormArray([], [Validators.required]),
      personnal_resources: new FormArray([], [Validators.required]),
      schemes: new FormArray([], [Validators.required]),
      scheme: new FormControl('', [Validators.required]),
      situation: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(150)]),
    })
    this.cleanFormGroup = this.corpusForm;
  }

  get date() {
    return this.corpusForm.get('date');
  }

  get activity() {
    return this.corpusForm.get('activity');
  }

  get action() {
    return this.corpusForm.get('action');
  }

  get actions(): FormArray {
    return (this.corpusForm.get('actions') as FormArray);
  }

  get actors(): FormArray {
    return (this.corpusForm.get('actors') as FormArray);
  }

  get performances(): FormArray {
    return (this.corpusForm.get('performances') as FormArray);
  }

  get objectives(): FormArray {
    return (this.corpusForm.get('objectives') as FormArray);
  }

  get schemes(): FormArray {
    return (this.corpusForm.get('schemes') as FormArray);
  }

  get environmental_resources(): FormArray {
    return (this.corpusForm.get('environmental_resources') as FormArray);
  }

  get personnal_resources(): FormArray {
    return (this.corpusForm.get('personnal_resources') as FormArray);
  }

  hasUnvalidatedTasks(tasks): boolean {
    return tasks.some(task => !task['validated']);
  }

  hasValidatedTasks(tasks): boolean {
    return tasks.some(task => task['validated']);
  }

  addField(formArray: FormArray, value: string[] = []) {
    let control;
    if (formArray === this.performances) {
      control = this.formBuilder.group({
        criterion: new FormControl(value['criterion'] ? value['criterion'] : '', [Validators.required]),
        value: new FormControl(value['value'] ? value['value'] : '', [Validators.required]),
        unit: new FormControl(value['unit'] ? value['unit'] : '', [Validators.required]),
      })
    } else if (formArray === this.environmental_resources || formArray === this.personnal_resources) {
      control = this.formBuilder.group({
        description: new FormControl(value['description'] ? value['description'] : '', [Validators.required]),
        type: new FormControl(value['type'] ? value['type'] : '', [Validators.required]),
      })
    } else if (formArray === this.objectives) {
      for (let i = this.objectives.length -1; i >= 0; i--) {
        const control = this.objectives.at(i);
        if (value === control.value.control) {
          this.objectives.removeAt(i);
        }
      }
      control = this.formBuilder.group({
        control: new FormControl(value, [Validators.required]),
      })
    } else {
      control = this.formBuilder.group({
        control: new FormControl(value, [Validators.required]),
      })
    }
    formArray.push(control);
  }

  removeField(formArray: FormArray, index: number) {
    formArray.removeAt(index);
  }

  loadAction(): void {
    const action = this.action.value;
    this.date.patchValue(action['date']);
    action['actions'].length ? action['actions'].forEach(el => this.addField(this.actions, el)) : this.addField(this.actions);
    action['actors'].length ? action['actors'].forEach(el => this.addField(this.actors, el)) : this.addField(this.actors);
    action['environmental_resources'].length ? action['environmental_resources'].forEach(el => this.addField(this.environmental_resources, el)) : this.addField(this.environmental_resources);
    action['personnal_resources'].length ? action['personnal_resources'].forEach(el => this.addField(this.personnal_resources, el)) : this.addField(this.personnal_resources);
    this.openAction = true;
  }

  unloadAction(): void {
    this.openAction = false;
    this.corpusForm = new FormGroup({
      date: new FormControl(''),
      activity: new FormControl(this.activity.value, [Validators.required, Validators.minLength(5), Validators.maxLength(150)]),
      action: new FormControl('', [Validators.required]),
      actions: new FormArray([], [Validators.required]),
      actors: new FormArray([], [Validators.required]),
      objectives: new FormArray([], [Validators.required]),
      performances: new FormArray([], [Validators.required]),
      environmental_resources: new FormArray([], [Validators.required]),
      personnal_resources: new FormArray([], [Validators.required]),
      schemes: new FormArray([], [Validators.required]),
      scheme: new FormControl('', [Validators.required]),
      situation: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(150)]),
    });
    (this.selectData['objectives'] && this.selectData['objectives'].length) ? this.selectData['objectives'].forEach(el => this.addField(this.objectives, el)) : this.addField(this.objectives);
    (this.selectData['performances'] && this.selectData['performances'].length) ? this.selectData['performances'].forEach(el => this.addField(this.performances, el)) : this.addField(this.performances);
    (this.selectData['schemes'] && this.selectData['schemes'].length) ? this.selectData['schemes'].forEach(el => this.addField(this.schemes, el)) : this.addField(this.schemes);
  }

  togglePreMap() {
    this.preMap = !this.preMap;
  }

  ngOnChanges(changes: SimpleChanges): void {
    const data = changes['selectData'].currentValue;
    this.activityIndex = data['index']
    this.corpusForm.patchValue({
      activity: data['activity'],
      situation: data['situation'],
    });
    
    (data['objectives'] && data['objectives'].length) ? data['objectives'].forEach(el => this.addField(this.objectives, el)) : this.addField(this.objectives);
    (data['performances'] && data['performances'].length) ? data['performances'].forEach(el => this.addField(this.performances, el)) : this.addField(this.performances);
    (data['schemes'] && data['schemes'].length) ? data['schemes'].forEach(el => this.addField(this.schemes, el)) : this.addField(this.schemes);
  }
}