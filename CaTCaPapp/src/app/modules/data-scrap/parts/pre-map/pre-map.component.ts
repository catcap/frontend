import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Network } from 'vis-network';
import { DataSet } from 'vis-data';
import { v4 as uuidv4 } from 'uuid';
import { faArrowLeft, faSave } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { DataService } from '../../../../services/data/data.service';

@Component({
  selector: 'app-pre-map',
  templateUrl: './pre-map.component.html',
  styleUrls: ['./pre-map.component.scss'],
})
export class PreMapComponent implements AfterViewInit, OnChanges {
  @Input() activityIndex: string;
  @Input() data: FormGroup;
  @Input() corpusId: string;
  @Input() corpusName: string;

  faArrowLeft = faArrowLeft as IconProp;
  faSave = faSave as IconProp;

  formatedData: object[] = [];
  network: Network;
  nodes = new DataSet([]);
  edges = new DataSet([]);
  edgesNum: number = 0;

  @Output() back: EventEmitter<void> = new EventEmitter();

  options: object = {
    autoResize: true,
    locale: 'fr',
    interaction: {
      multiselect: false,
    },
    manipulation: {
      enabled: false,
      initiallyActive: false,
    },
    physics: {
      forceAtlas2Based: {
        gravitationalConstant: -50,
        centralGravity: 0.01,
        springLength: 100,
        damping: 0.4,
        avoidOverlap: 1,
      },
      minVelocity: 0.75,
      solver: 'forceAtlas2Based',
    },
    nodes: {
      chosen: true,
      mass: 2,
      color: {
        border: '#000000',
        highlight: {
          border: '#40E0D0',
        },
      },
      font: {
        size: 14,
        color: '#000000',
      },
      shadow: {
        color: '#00000080',
        size: 10,
        x: 5,
        y: 5,
      },
      shape: 'circle',
      widthConstraint: {
        minimum: 100,
        maximum: 150,
      },
    },
    edges: {
      chosen: true,
      color: {
        highlight: '#40E0D0',
      },
      arrows: {
        to: {
          enabled: false,
          scaleFactor: 0.5,
        },
      },
      smooth: false,
      selectionWidth: (width: number) => {
        return width * 5;
      },
      width: 1,
      length: 150,
    },
  };

  @ViewChild('networkContainer') networkContainer: any;

  constructor(private dataService: DataService) {}

  updateData(data): void {
    this.formatedData = data;
  }

  addNewEdge(edge): void {
    this.edges.add(edge);
  }

  removeEdge(edge): void {
    this.edges.remove(
      this.edges
        .get()
        .find((elem) => elem.from === edge['from'] && elem.to === edge['to'])
    );
  }

  goBack(): void {
    this.back.emit();
  }

  validateCorpus(): void {
    const workCorpus = {
      activity: this.data.value['activity'],
      objectives: this.data.value['objectives'].map((elem) => elem.control),
      performances: this.data.value['performances'][0]['criterion']
        ? this.data.value['performances']
        : [],
      schemes: this.data.value['schemes'].map((elem) => elem.control),
      situation: this.data.value['situation']
        ? this.data.value['situation']
        : null,
      task: {
        actors: this.data.value['actors'].map((elem) => elem.control),
        actions: this.data.value['actions'].map((elem) => elem.control),
        environmental_resources: this.data.value['environmental_resources'][0][
          'description'
        ]
          ? this.data.value['environmental_resources']
          : [],
        personnal_resources: this.data.value['personnal_resources'][0][
          'description'
        ]
          ? this.data.value['personnal_resources']
          : [],
      },
    };
    const workCorpusJsonString = JSON.stringify(workCorpus);
    const workCorpusBlob = new Blob([workCorpusJsonString], {
      type: 'application/json',
    });
    const mapJsonString = JSON.stringify(this.formatedData);
    const mapBlob = new Blob([mapJsonString], { type: 'application/json' });
    const formData = new FormData();
    formData.append('file', mapBlob, 'file.json');
    formData.append('work_corpus', workCorpusBlob, 'workCorpus.json');
    this.dataService
      .validateData(
        `${this.corpusId}/${this.activityIndex}/${this.data.value['action']['index']}`,
        formData
      )
      .subscribe({
        complete: () => this.dataService.refresh(),
      });
  }

  ngAfterViewInit(): void {
    this.network = new Network(
      this.networkContainer.nativeElement,
      { nodes: this.nodes, edges: this.edges },
      this.options
    );
    this.dataService.getPreMap(this.corpusName, this.corpusId).subscribe({
      next: (res) => {
        res['data'].forEach((data) => {
          if (!data['labels'].includes('Proxyma_Corpus')) {
            let alreadyExists = this.formatedData.find((el) => {
              return Object.keys(data['properties']).every((key) => {
                if (
                  ![
                    'uid',
                    'background',
                    'highlight',
                    'created_at',
                    'deleted_at',
                    'type',
                  ].includes(key) &&
                  key !== undefined
                ) {
                  return (
                    el['properties'][key]?.toLowerCase() ===
                    data['properties'][key]?.toLowerCase()
                  );
                }
                return true;
              });
            });

            if (alreadyExists) {
              const corpusRelationships = this.formatedData.find((el) =>
                el['labels'].includes('Proxyma_Corpus')
              )['relationships'];
              for (let part in corpusRelationships) {
                const index = corpusRelationships[part].findIndex(
                  (el) => el === alreadyExists['properties']['uid']
                );
                if (index >= 0) {
                  corpusRelationships[part].splice(index, 1);
                }
              }
              this.nodes.remove(alreadyExists['properties']['uid']);

              for (let node of this.formatedData) {
                for (let part in node['relationships']) {
                  const index = node['relationships'][part].findIndex(
                    (el) => el === alreadyExists['properties']['uid']
                  );
                  if (index > -1) {
                    node['relationships'][part][index] =
                      data['properties']['uid'];
                  }
                }
              }

              alreadyExists['properties'] = data['properties'];
              for (let elem in alreadyExists['relationships']) {
                for (let uid of data['relationships'][elem]) {
                  if (!alreadyExists['relationships'][elem].includes(uid)) {
                    alreadyExists['relationships'][elem].push(uid);
                  }
                }
              }
            } else {
              this.formatedData.push(data);
            }
          } else {
            this.formatedData.find((el) =>
              el['labels'].includes('Proxyma_Corpus')
            )['properties']['uid'] = data['properties']['uid'];
            const relationships = this.formatedData.find((el) =>
              el['labels'].includes('Proxyma_Corpus')
            )['relationships'];
            for (const key in relationships) {
              relationships[key].push(...data['relationships'][key]);
            }
          }
        });
      },
      complete: () => {
        for (let data of this.formatedData) {
          if (!data['labels'].includes('Proxyma_Corpus')) {
            this.nodes.add({
              id: data['properties']['uid'],
              label:
                data['properties']['description'] ||
                data['properties']['name'] ||
                data['properties']['actions_sequence'] ||
                data['properties']['criterion'] +
                  ' : ' +
                  data['properties']['value'] +
                  ' ' +
                  data['properties']['unit'],
              type: data['labels'][0],
              begin: data['properties']['created_at'],
              end: data['properties']['deleted_at'],
              color: {
                background: data['properties']['background'],
                highlight: {
                  background: data['properties']['highlight'],
                },
              },
            });

            for (let rel in data['relationships']) {
              for (let uid of data['relationships'][rel]) {
                this.edges.add({
                  from: data['properties']['uid'],
                  to: uid,
                });
                this.edgesNum++;
              }
            }
          }
          
        }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    const data = changes['data'].currentValue;
    this.formatedData.push({
      labels: ['Proxyma_Corpus'],
      properties: {
        uid: uuidv4(),
        title: this.corpusName,
      },
      relationships: {
        actions: [],
        activities: [],
        actors: [],
        environmental_resources: [],
        objectives: [],
        performances: [],
        personnal_resources: [],
        schemes: [],
        situations: [],
      },
    });
    for (let actor of data.value['actors']) {
      if (actor['control'] !== '') {
        const uid = uuidv4();
        this.formatedData[0]['relationships']['actors'].push(uid);
        this.formatedData.push({
          labels: ['Actor'],
          properties: {
            uid: uid,
            name: actor['control'],
            background: '#FABC1C',
            highlight: '#FFB700',
            created_at: data.value['date']
          },
          relationships: {
            personnal_resources: [],
          },
        });
      }
    }
    for (let action of data.value['actions']) {
      if (action['control'] !== '') {
        const uid = uuidv4();
        this.formatedData[0]['relationships']['actions'].push(uid);
        this.formatedData.push({
          labels: ['Action'],
          properties: {
            uid: uid,
            description: action['control'],
            background: '#AAAAAA',
            highlight: '#FFFFFF',
            created_at: data.value['date'],
          },
          relationships: {},
        });
      }
    }
    if (data.value['activity'] !== '') {
      const uid = uuidv4();
      this.formatedData[0]['relationships']['activities'].push(uid);
      this.formatedData.push({
        labels: ['Activity'],
        properties: {
          uid: uid,
          description: data.value['activity'],
          background: '#549CDD',
          highlight: '#0087FF',
          created_at: data.value['date'],
        },
        relationships: {
          objectives: [],
          schemes: [],
          situations: [],
        },
      });
    }
    for (let environmental_resource of data.value['environmental_resources']) {
      if (environmental_resource['description'] !== '') {
        const uid = uuidv4();
        this.formatedData[0]['relationships']['environmental_resources'].push(
          uid
        );
        this.formatedData.push({
          labels: ['Environmental_Resource'],
          properties: {
            uid: uid,
            description: environmental_resource['description'],
            type: environmental_resource['type'],
            background: '#6EC667',
            highlight: '#11FF00',
            created_at: data.value['date'],
          },
          relationships: {},
        });
      }
    }
    for (let objective of data.value['objectives']) {
      if (objective['control'].length > 0) {
        const uid = uuidv4();
        this.formatedData[0]['relationships']['objectives'].push(uid);
        this.formatedData.push({
          labels: ['Objective'],
          properties: {
            uid: uid,
            description: objective['control'],
            background: '#E95615',
            highlight: '#FF5000',
            created_at: data.value['date'],
          },
          relationships: {
            performances: [],
          },
        });
      }
    }
    for (let performance of data.value['performances']) {
      if (
        performance['criterion'] !== '' &&
        performance['value'] !== '' &&
        performance['unit'] !== ''
      ) {
        const uid = uuidv4();
        this.formatedData[0]['relationships']['performances'].push(uid);
        this.formatedData.push({
          labels: ['Performance'],
          properties: {
            uid: uid,
            criterion: performance['criterion'],
            value: performance['value'],
            unit: performance['unit'],
            background: '#F4A09F',
            highlight: '#FF0200',
            created_at: data.value['date'],
          },
          relationships: {},
        });
      }
    }
    for (let personnal_resource of data.value['personnal_resources']) {
      if (personnal_resource['description'] !== '') {
        const uid = uuidv4();
        this.formatedData[0]['relationships']['personnal_resources'].push(uid);
        this.formatedData.push({
          labels: ['Personnal_Resource'],
          properties: {
            uid: uid,
            type: personnal_resource['type'],
            description: personnal_resource['description'],
            background: '#F79767',
            highlight: '#FF97AA',
            created_at: data.value['date'],
          },
          relationships: {},
        });
      }
    }
    if (data.value['scheme']) {
      const uid = uuidv4();
      this.formatedData[0]['relationships']['schemes'].push(uid);
      this.formatedData.push({
        labels: ['Scheme'],
        properties: {
          uid: uid,
          actions_sequence: data.value['scheme'],
          background: '#00CCFF',
          highlight: '#57C7E3',
          created_at: data.value['date'],
        },
        relationships: {
          actions: [],
          actors: [],
          environmental_resources: [],
        },
      });
    }
    if (data.value['situation']) {
      const uid = uuidv4();
      this.formatedData[0]['relationships']['situations'].push(uid);
      this.formatedData.push({
        labels: ['Situation'],
        properties: {
          uid: uid,
          description: data.value['situation'],
          background: '#D9D9D9',
          highlight: '#F0F0F0',
          created_at: data.value['date'],
        },
        relationships: {},
      });
    }

    for (let data of this.formatedData) {
      switch (data['labels'][0]) {
        case 'Actor':
          data['relationships']['personnal_resources'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Personnal_Resource')
            .map((elem) => elem['properties']['uid']);
          break;
        case 'Activity':
          data['relationships']['objectives'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Objective')
            .map((elem) => elem['properties']['uid']);
          data['relationships']['schemes'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Scheme')
            .map((elem) => elem['properties']['uid']);
          data['relationships']['situations'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Situation')
            .map((elem) => elem['properties']['uid']);
          break;
        /* case 'Objective':
          data['relationships']['performances'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Performance')
            .map((elem) => elem['properties']['uid']);
          break; */
        case 'Scheme':
          data['relationships']['actions'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Action')
            .map((elem) => elem['properties']['uid']);
          data['relationships']['actors'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Actor')
            .map((elem) => elem['properties']['uid']);
          data['relationships']['environmental_resources'] = this.formatedData
            .filter((elem) => elem['labels'][0] === 'Environmental_Resource')
            .map((elem) => elem['properties']['uid']);
          break;
        default:
          break;
      }
    }
  }
}
