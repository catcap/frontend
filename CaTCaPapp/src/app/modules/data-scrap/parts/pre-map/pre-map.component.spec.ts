import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreMapComponent } from './pre-map.component';

describe('PreMapComponent', () => {
  let component: PreMapComponent;
  let fixture: ComponentFixture<PreMapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PreMapComponent]
    });
    fixture = TestBed.createComponent(PreMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
