import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreMapEditComponent } from './pre-map-edit.component';

describe('PreMapEditComponent', () => {
  let component: PreMapEditComponent;
  let fixture: ComponentFixture<PreMapEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PreMapEditComponent]
    });
    fixture = TestBed.createComponent(PreMapEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
