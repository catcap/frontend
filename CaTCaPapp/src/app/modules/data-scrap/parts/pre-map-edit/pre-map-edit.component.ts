import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import {
  faAngleDown,
  faAngleUp,
  faArrowLeft,
  faCheck,
  faMinus,
  faPlus,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-pre-map-edit',
  templateUrl: './pre-map-edit.component.html',
  styleUrls: ['./pre-map-edit.component.scss'],
})
export class PreMapEditComponent {
  @Input() data: object[];
  @Input() edgeNumber: number;

  faAngle = faAngleDown as IconProp;
  faArrowLeft = faArrowLeft as IconProp;
  faCheck = faCheck as IconProp;
  faMinus = faMinus as IconProp;
  faPlus = faPlus as IconProp;

  editorToggle: boolean = false;
  state: string | undefined;
  edgeForm: FormGroup;

  @Output() newEdge: EventEmitter<object> = new EventEmitter();
  @Output() detachNode: EventEmitter<object> = new EventEmitter();
  @Output() formatedData: EventEmitter<object[]> = new EventEmitter();

  constructor() {
    this.edgeForm = new FormGroup({
      from: new FormControl(null, [Validators.required]),
      to: new FormControl(null, [Validators.required]),
    });
  }

  setState(state: string | undefined = undefined): void {
    this.state = state;
    if (state === undefined) this.edgeForm.reset();
  }

  toggleEditor(): void {
    this.faAngle = this.editorToggle
      ? (faAngleDown as IconProp)
      : (faAngleUp as IconProp);
    this.editorToggle = !this.editorToggle;
  }

  getFromPermission(nodeType: string): boolean {
    if (['Activity', 'Actor', 'Objective', 'Scheme'].includes(nodeType))
      return true;
    else return false;
  }

  getToPermission(nodeType: string): object[] {
    const authorizedNodes: object[] = [];
    for (let node of this.data) {
      switch (nodeType) {
        case 'Activity':
          if (['Objective', 'Scheme', 'Situation'].includes(node['labels'][0]))
            authorizedNodes.push(node);
          break;
        case 'Actor':
          if (['Personnal_Resource'].includes(node['labels'][0]))
            authorizedNodes.push(node);
          break;
        case 'Objective':
          if (['Performance'].includes(node['labels'][0]))
            authorizedNodes.push(node);
          break;
        case 'Scheme':
          if (
            ['Action', 'Actor', 'Environmental_Resource'].includes(
              node['labels'][0]
            )
          )
            authorizedNodes.push(node);
          break;
        default:
          break;
      }
    }
    return authorizedNodes;
  }

  isConnected(from: Object, to: Object): boolean {
    return Object.values(from['relationships']).some((arr: string[]) =>
      arr.includes(to['properties']['uid'])
    );
  }

  connectNodes(): void {
    const node1 = this.edgeForm.get('from').value;
    const node2 = this.edgeForm.get('to').value;
    for (let node of this.data) {
      if (node['properties']['uid'] === node1['properties']['uid']) {
        switch (node2['labels'][0]) {
          case 'Action':
            node['relationships']['actions'].push(node2['properties']['uid']);
            break;
          case 'Actor':
            node['relationships']['actors'].push(node2['properties']['uid']);
            break;
          case 'Environmental_Resource':
            node['relationships']['environmental_resources'].push(
              node2['properties']['uid']
            );
            break;
          case 'Objective':
            node['relationships']['objectives'].push(
              node2['properties']['uid']
            );
            break;
          case 'Performance':
            node['relationships']['performances'].push(
              node2['properties']['uid']
            );
            break;
          case 'Personnal_Resource':
            node['relationships']['personnal_resources'].push(
              node2['properties']['uid']
            );
            break;
          case 'Scheme':
            node['relationships']['schemes'].push(node2['properties']['uid']);
            break;
          case 'Situation':
            node['relationships']['situations'].push(
              node2['properties']['uid']
            );
            break;
          default:
            break;
        }
      }
    }

    const edge = {
      from: node1['properties']['uid'],
      to: node2['properties']['uid'],
    };
    this.newEdge.emit(edge);
    this.formatedData.emit(this.data);
    this.edgeNumber++;
    this.edgeForm.get('to').reset();
  }

  disconnectNodes(): void {
    const node1 = this.edgeForm.get('from').value;
    const node2 = this.edgeForm.get('to').value;
    for (let node of this.data) {
      if (node['properties']['uid'] === node1['properties']['uid']) {
        switch (node2['labels'][0]) {
          case 'Action':
            node['relationships']['actions'].splice(
              node['relationships']['actions'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          case 'Actor':
            node['relationships']['actors'].splice(
              node['relationships']['actors'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          case 'Environmental_Resource':
            node['relationships']['environmental_resources'].splice(
              node['relationships']['environmental_resources'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          case 'Objective':
            node['relationships']['objectives'].splice(
              node['relationships']['objectives'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          case 'Performance':
            node['relationships']['performances'].splice(
              node['relationships']['performances'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          case 'Personnal_Resource':
            node['relationships']['personnal_resources'].splice(
              node['relationships']['personnal_resources'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          case 'Scheme':
            node['relationships']['schemes'].splice(
              node['relationships']['schemes'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          case 'Situation':
            node['relationships']['situations'].splice(
              node['relationships']['situations'].findIndex(
                (elem) => elem === node2['properties']['uid']
              ),
              1
            );
            break;
          default:
            break;
        }
      }
    }

    const edge = {
      from: node1['properties']['uid'],
      to: node2['properties']['uid'],
    };
    this.detachNode.emit(edge);
    this.formatedData.emit(this.data);
    this.edgeNumber--;
    this.edgeForm.get('to').reset();
  }
}
