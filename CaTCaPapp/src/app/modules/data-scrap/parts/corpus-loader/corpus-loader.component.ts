import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-corpus-loader',
  templateUrl: './corpus-loader.component.html',
  styleUrls: ['./corpus-loader.component.scss'],
})
export class CorpusLoaderComponent {
  faCheck = faCheck as IconProp;
  corpus: String[] = [];
  dataSetForm = this.formBuilder.group({
    corpus: [null, [Validators.required]]
  })

  @Output() loadSignal: EventEmitter<object> = new EventEmitter();

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder
  ) {
    this.getCorpus();
    this.dataService.reloadCorpus.subscribe({
      next: () => this.getCorpus(),
    });
  }

  getCorpus() {
    this.dataSetForm.patchValue({ corpus: null })
    this.dataService.getCorpusNames().subscribe({
      next: (res) => {
        this.corpus = res;
      },
      error: (error) => console.error(error),
    });
  }

  onSubmit() {
    this.loadSignal.emit(this.dataSetForm.value.corpus);
  }
}
