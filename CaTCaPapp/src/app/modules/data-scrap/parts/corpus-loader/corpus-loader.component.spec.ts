import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorpusLoaderComponent } from './corpus-loader.component';

describe('CorpusLoaderComponent', () => {
  let component: CorpusLoaderComponent;
  let fixture: ComponentFixture<CorpusLoaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CorpusLoaderComponent]
    });
    fixture = TestBed.createComponent(CorpusLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
