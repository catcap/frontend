import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data/data.service';
import * as FileSaver from "file-saver";

@Component({
  selector: 'app-data-viewer',
  templateUrl: './data-viewer.component.html',
  styleUrls: ['./data-viewer.component.scss']
})
export class DataViewerComponent{
  @Input() data: Object;
  @Input() corpusId: string;
  @Input() corpusName: string;
  openDataViz: boolean = false;
  selectData: object | null = null;

  @Output() clear: EventEmitter<boolean> = new EventEmitter();
  
  constructor(
    private dataService: DataService,
  ) {
    this.dataService.refreshData.subscribe({
      next: () => this.dataService.getData(this.corpusId).subscribe({
        next: (res) => {
          this.data = res['data'];
          this.corpusId = res['id'];
          this.corpusName = res['corpus'];
        }
      })
    })
  }
  
  getRawData() {
    this.dataService.getRawData(this.corpusId).subscribe({
      next: (res) => {
        const preProcessJsonString = JSON.stringify(res['pre_process_data']);
        const preProcessBlob = new Blob([preProcessJsonString], { type: "application/json" });
        FileSaver.saveAs(preProcessBlob, `${this.corpusName}_pre_process_data.json`);
        const postProcessJsonString = JSON.stringify(res['post_process_data']);
        const postProcessBlob = new Blob([postProcessJsonString], { type: "application/json" });
        FileSaver.saveAs(postProcessBlob, `${this.corpusName}_post_process_data.json`);
      }
    })
  }

  deleteCorpus() {
    this.dataService.deleteCorpus(this.corpusId).subscribe({
      complete: () => this.clear.emit(true),
    });
    
  }

  dataVizToggle(status: boolean, data: object | null = null) {
    this.openDataViz = status;
    this.selectData = data;
  }
}
