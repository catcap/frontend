import { DOCUMENT } from '@angular/common';
import {
	AfterViewInit,
	Component,
	Inject,
} from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import {
	faArrowLeft,
	faArrowRight,
	faCircleRight,
} from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'app-carousel',
	templateUrl: './carousel.component.html',
	styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements AfterViewInit {
	arrowLeft = faArrowLeft as IconProp;
	arrowRight = faArrowRight as IconProp;
	circleRight = faCircleRight as IconProp;
	

	status: string = null;

	toggleStatus(status: string = null): void {
		this.status = status;
	}

	constructor(@Inject(DOCUMENT) private document: Document) { }

	ngAfterViewInit(): void {
		const sliderWrapper: HTMLElement = this.document.querySelector('.slider-wrapper'),
			slide = this.document.querySelector('.slide'),
			allSlides = this.document.querySelectorAll('.slide'),
			previousArrow = this.document.querySelector('.slider-prev'),
			nextArrow = this.document.querySelector('.slider-next');

		let counter: number = 0,
			sliderWrapperLength: number = window.innerWidth < 640
				? Array.from(sliderWrapper.children).length - 1
				: Array.from(sliderWrapper.children).length - 3,
			slideWidth: number = slide.getBoundingClientRect().width,
			gap: string,
			posClick: number = 0,
			posSwitch: number = 0,
			posInit: number,
			posFinal: number,
			trigger: number,
			allSlidesWidth: number = window.innerWidth < 640
				? slide.getBoundingClientRect().width * (allSlides.length - 1)
				: slide.getBoundingClientRect().width * (allSlides.length - 3);



		// Arrows directed carousel

		function previous() {
			counter--;
			counter = counter < 0 ? sliderWrapperLength : counter;
			sliderWrapper.style.transition =
				counter < 0 ? 'left .6s unset' : 'left .6s linear';
			gap =
				counter < 0
					? `${slideWidth * counter}px`
					: `${-slideWidth * counter}px`;
			sliderWrapper.style.left = gap;
		}

		function next() {
			counter++;
			counter = counter >= sliderWrapperLength + 1 ? 0 : counter;
			sliderWrapper.style.transition =
				counter > sliderWrapperLength + 1
					? 'left .6s unset'
					: 'left .6s linear';
			gap =
				counter > sliderWrapperLength + 1 ? '0' : `-${slideWidth * counter}px`;
			sliderWrapper.style.left = gap;
		}

		previousArrow.addEventListener('click', previous);
		nextArrow.addEventListener('click', next);


		// Handle screen resizing

		window.addEventListener('resize', () => {
			sliderWrapperLength = window.innerWidth < 640
				? Array.from(sliderWrapper.children).length - 1
				: Array.from(sliderWrapper.children).length - 3,
				slideWidth = slide.getBoundingClientRect().width,
				allSlidesWidth = window.innerWidth < 640
					? slide.getBoundingClientRect().width * (allSlides.length - 1)
					: slide.getBoundingClientRect().width * (allSlides.length - 3);

			next();
		})
	}
}
