import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTreatmentComponent } from './data-treatment.component';

describe('DataTreatmentComponent', () => {
  let component: DataTreatmentComponent;
  let fixture: ComponentFixture<DataTreatmentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DataTreatmentComponent]
    });
    fixture = TestBed.createComponent(DataTreatmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
