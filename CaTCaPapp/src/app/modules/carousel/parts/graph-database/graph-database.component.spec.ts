import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphDatabaseComponent } from './graph-database.component';

describe('GraphDatabaseComponent', () => {
  let component: GraphDatabaseComponent;
  let fixture: ComponentFixture<GraphDatabaseComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GraphDatabaseComponent]
    });
    fixture = TestBed.createComponent(GraphDatabaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
