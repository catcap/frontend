import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BreakpointsService } from 'src/app/services/breakpoints/breakpoints.service';
import { ContentService } from 'src/app/services/content/content.service';

@Component({
  selector: 'app-map-tuto',
  templateUrl: './map-tuto.component.html',
  styleUrls: ['./map-tuto.component.scss']
})
export class MapTutoComponent implements OnInit {
  contents: object[] = [];
  breakpointsSubscription: Subscription | undefined;
  height: number | undefined;
  width: number | undefined;

  constructor(private breakpointsService: BreakpointsService, private contentService: ContentService) {
    this.contentService.getContent('MB').subscribe({
      next: (res) => this.contents = res['data'],
      complete: () => this.contents.sort((a, b) => a['order'] - b['order'])
    })
  }

  ngOnInit(): void {
    this.breakpointsSubscription = this.breakpointsService.getTypeObservable().subscribe((type) => {
      switch(type) {
        case 'xs':
          this.height = 80;
          this.width = 142;
          break;
        case 'md':
          this.height = 120;
          this.width = 214;
          break;
        case 'lg':
          this.height = 160;
          this.width = 284;
          break;
          case 'xl':
            this.height = 220;
            this.width = 392;
            break;
          default:
            break;
      }
    });
  }
}
