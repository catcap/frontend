import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapTutoComponent } from './map-tuto.component';

describe('MapTutoComponent', () => {
  let component: MapTutoComponent;
  let fixture: ComponentFixture<MapTutoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MapTutoComponent]
    });
    fixture = TestBed.createComponent(MapTutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
