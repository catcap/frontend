import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LowLevelTracesComponent } from './low-level-traces.component';

describe('LowLevelTracesComponent', () => {
  let component: LowLevelTracesComponent;
  let fixture: ComponentFixture<LowLevelTracesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LowLevelTracesComponent]
    });
    fixture = TestBed.createComponent(LowLevelTracesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
