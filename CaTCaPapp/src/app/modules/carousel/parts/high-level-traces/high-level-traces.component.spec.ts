import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HighLevelTracesComponent } from './high-level-traces.component';

describe('HighLevelTracesComponent', () => {
  let component: HighLevelTracesComponent;
  let fixture: ComponentFixture<HighLevelTracesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HighLevelTracesComponent]
    });
    fixture = TestBed.createComponent(HighLevelTracesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
