import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselComponent } from './carousel.component';
import { DataTreatmentComponent } from './parts/data-treatment/data-treatment.component';
import { GraphDatabaseComponent } from './parts/graph-database/graph-database.component';
import { HighLevelTracesComponent } from './parts/high-level-traces/high-level-traces.component';
import { LowLevelTracesComponent } from './parts/low-level-traces/low-level-traces.component';
import { MapTutoComponent } from './parts/map-tuto/map-tuto.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { YouTubePlayerModule } from '@angular/youtube-player';


@NgModule({
  declarations: [
    CarouselComponent,
    DataTreatmentComponent,
    GraphDatabaseComponent,
    HighLevelTracesComponent,
    LowLevelTracesComponent,
    MapTutoComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    YouTubePlayerModule,
    RouterModule.forChild([{path: '', component: CarouselComponent}])
  ],
  exports: [
    CarouselComponent,
  ]
})
export class CarouselModule { }
