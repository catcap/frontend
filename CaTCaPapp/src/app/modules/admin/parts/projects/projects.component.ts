import { Component } from '@angular/core';
import { faArrowRotateBack, faCheck, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { ProxymaService } from 'src/app/services/proxyma/proxyma.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
	selector: 'app-projects',
	templateUrl: './projects.component.html',
	styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {
	faArrowRotateBack = faArrowRotateBack as IconProp;
	faCheck = faCheck as IconProp;
	faEdit = faEdit as IconProp;
	faTrash = faTrash as IconProp;
	feedback: string = null;
	deleteCorpus: string = null;
	proxyma_list: object[] = [];
	updateCorpus: object = null;
	corpusTitleForm: FormGroup;

	constructor(
		private proxymaService: ProxymaService,
	) {
		this.corpusTitleForm = new FormGroup({
			corpus_title: new FormControl('', [Validators.required])
		})
		this.getProxymaList();
		this.proxymaService.reloadProxyma.subscribe({
			next: () => this.getProxymaList(),
		})
	}

	toggleDeleteCorpus(corpus_id: string = null): void {
		this.deleteCorpus = corpus_id;
		if (corpus_id === null) this.getProxymaList();
	}

	toggleUpdateCorpus(corpus: object = null): void {
		this.updateCorpus = corpus;
	}

	submitUpdateCorpus(): void {
		this.proxymaService.updateProxymaTitle(this.updateCorpus['id'], this.corpusTitleForm.value).subscribe({
			error: (err) => this.feedback = "error",
			complete: () => {
				this.feedback = "success";
				this.getProxymaList();
				setTimeout(() => {
					this.feedback = null;
					this.toggleUpdateCorpus();
				}, 5000)
			},
		})
	}

	getProxymaList() {
		this.proxymaService.getProxymaList().subscribe({
			next: (res: object[]) => {
				this.proxyma_list = [];
				this.proxyma_list = res;
			},
		})
	}
}
