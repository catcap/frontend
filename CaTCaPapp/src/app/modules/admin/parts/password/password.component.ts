import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ConfirmPasswordValidator } from 'src/app/validators/confirm-password.validator';
import { passwordValidator } from 'src/app/validators/password.validator';

@Component({
	selector: 'app-password',
	templateUrl: './password.component.html',
	styleUrls: ['./password.component.scss']
})
export class PasswordComponent {
	feedback: object | null;
	passwordForm: FormGroup;
	@Output() rollback: EventEmitter<void> = new EventEmitter();

	constructor(
		private authService: AuthenticationService,
	) {
		this.passwordForm = new FormGroup({
			old_password: new FormControl('', [Validators.required, passwordValidator]),
			password: new FormControl('', [Validators.required, passwordValidator]),
			confirmPassword: new FormControl('', [Validators.required]),
		}, {
			validators: ConfirmPasswordValidator
		})
	}

	get old_password() {
		return this.passwordForm.get('old_password');
	}

	get password() {
		return this.passwordForm.get('password');
	}

	get confirmPassword() {
		return this.passwordForm.get('confirmPassword');
	}

	onPasswordSubmit() {
		this.authService.changePassword(this.passwordForm.value).subscribe({
			next: () => this.feedback = { status: true, message: 'Mot de passe mis à jour' },
			error: () => this.feedback = { status: false, message: 'Erreur lors de la mise à jour du mot de passe' },
			complete: () => {
				setTimeout(() => {
					this.feedback = null;
				}, 5000);
				this.goBack();
			}
		})
	}

	goBack() {
		this.rollback.emit();
	}
}
