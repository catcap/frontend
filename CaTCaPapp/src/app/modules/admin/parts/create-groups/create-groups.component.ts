import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GroupService } from 'src/app/services/group/group.service';
import { faX } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

@Component({
	selector: 'app-create-groups',
	templateUrl: './create-groups.component.html',
	styleUrls: ['./create-groups.component.scss']
})
export class CreateGroupsComponent {
	user: string | null;
	users: string[] = [];
	groupForm: FormGroup;
	feedback: object;
	faX = faX as IconProp;

	@Output() rollback: EventEmitter<boolean> = new EventEmitter();
	constructor(
		private groupService: GroupService,
	) {
		this.groupForm = new FormGroup({
			group_name: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]),
			group_status: new FormControl(true, [Validators.required])
		})
	}

	get group_name(): any {
		return this.groupForm.get('group_name');
	}

	get group_status(): any {
		return this.groupForm.get('group_status');
	}


	updateUsers(user: string | null = null) {
		if (user === null) {
			this.users.push(this.user)
			this.user = null;
		} else {
			let index = this.users.findIndex((el) => el === user)
			this.users.splice(index, 1);
		}
	}

	updateStatus(status: boolean) {
		this.groupForm.patchValue({ group_status: status });
	}

	addGroup(): void {
		this.groupService.addgroup(this.groupForm.value, this.users).subscribe({
			complete: () => this.goBack(true),
		})
	}

	goBack(status: boolean = false): void {
		this.rollback.emit(status);
	}
}
