import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faEdit, faArrowRotateBack, faPlus, faSave, faTrash, faX } from '@fortawesome/free-solid-svg-icons';
import { ObservableInput, catchError } from 'rxjs';
import { GroupService } from 'src/app/services/group/group.service';

@Component({
	selector: 'app-group-details',
	templateUrl: './group-details.component.html',
	styleUrls: ['./group-details.component.scss']
})
export class GroupDetailsComponent {
	@Input() selectedGroup: object;
	faEdit = faEdit as IconProp;
	faArrowRotateBack = faArrowRotateBack as IconProp;
	faPlus = faPlus as IconProp;
	faSave = faSave as IconProp;
	faTrash = faTrash as IconProp;
	faX = faX as IconProp;
	formGroupName: FormGroup;
	formGroupUser: FormGroup;
	formGroupStatus: FormGroup;
	editField: string | null = null;
	deleteGroup: boolean = false;
	leaveGroup: boolean = false;

	feedBack: string = null;

	ownerError: boolean = false;
	noUserFoundError: boolean = false;

	@Output() rollback: EventEmitter<boolean> = new EventEmitter();

	constructor(
		private groupService: GroupService,
	) {
		this.formGroupName = new FormGroup({
			groupName: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]),
		});
		this.formGroupUser = new FormGroup({
			member: new FormControl('', [Validators.required, Validators.email]),
		})
		this.formGroupStatus = new FormGroup({
			status: new FormControl(''),
		})
	}

	editGroupToggle(field: string): void {
		if (this.editField === field) {
			this.editField = null;
			return;
		}
		this.editField = field;
		switch (field) {
			case 'groupName':
				this.formGroupName.patchValue({ groupName: this.selectedGroup['name'] })
				break;
			case 'groupMembers':
				this.formGroupUser.patchValue({ member: '' })
				break;
			case 'groupStatus':
				this.formGroupStatus.patchValue({ status: this.selectedGroup['status'] })
				break;
			default:
				break;
		}
	}

	get groupName(): any {
		return this.formGroupName.get('groupName')
	}

	get member(): any {
		return this.formGroupUser.get('member');
	}

	get status(): any {
		return this.formGroupStatus.get('status');
	}

	set status(value: boolean) {
		this.formGroupStatus.patchValue({ status: value })
		this.updateGroup()
	}

	updateGroup(member: string | null = null) {
		switch (this.editField) {
			case 'groupName':
				this.groupService.updategroup(this.selectedGroup['id'], { group_name: this.formGroupName.get('groupName').value }).subscribe({
					next: () => {
						this.selectedGroup['name'] = this.formGroupName.get('groupName').value;
					},
					complete: () => this.editGroupToggle('groupName')
				})
				break;
			case 'groupMembers':
				this.groupService.updategroup(this.selectedGroup['id'], { group_member: member === null ? this.formGroupUser.get('member').value : member }).subscribe({
					next: (res) => {
						const index = this.selectedGroup['members'].findIndex((el) => el === (member === null ? this.formGroupUser.get('member').value : member));
						if (index >= 0) {
							this.selectedGroup['members'].splice(index, 1);
						} else {
							this.selectedGroup['members'].push(res['user']);
						}
					},
					error: (err) => {
						switch (err["error"]) {
							case 'cannot_leave_owned_group':
								this.controlFeedBack("ownerError");
								break;
							case 'user_unknown':
								this.controlFeedBack("noUserFoundError");
								break;
							case 'user_already_in_group':
								this.controlFeedBack("userAlreadyGroupedError")
								break;
							default:
								break;
						}
					}
				});
				break;
			case 'groupStatus':
				this.groupService.updategroup(this.selectedGroup['id'], { group_status: this.formGroupStatus.get('status').value }).subscribe({
					next: () => this.selectedGroup['status'] = this.formGroupStatus.get('status').value,
					complete: () => this.editGroupToggle('groupStatus'),
				})
				break;
		}
	}

	controlFeedBack(msg: string): void {
		this.feedBack = msg;
		setTimeout(() => this.feedBack = null, 5000);
	}

	leaveToggle(): void {
		this.leaveGroup = !this.leaveGroup;
	}

	leaveGp(): void {
		this.groupService.updategroup(this.selectedGroup['id']).subscribe({
			complete: () => this.goBack(true)
		})
	}

	deleteToggle(): void {
		this.deleteGroup = !this.deleteGroup;
	}

	deleteGp(): void {
		const delGp = this.groupService.deleteSubgroup(this.selectedGroup['id'])
			.subscribe({
				error: (err) => {
					switch (err["error"]) {
						case "last_group_cant_be_deleted":
							this.controlFeedBack("lastGroupError");
							break;
						case "permission_denied":
							this.controlFeedBack("forbidenError");
							break;
						default:
							break;
					}
				},
				complete: () => this.goBack(true)
			})
	}

	goBack(del: boolean = false): void {
		this.rollback.emit(del);
	}
}
