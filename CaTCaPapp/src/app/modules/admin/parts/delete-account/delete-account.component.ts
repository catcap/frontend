import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { passwordValidator } from 'src/app/validators/password.validator';

@Component({
	selector: 'app-delete-account',
	templateUrl: './delete-account.component.html',
	styleUrls: ['./delete-account.component.scss']
})
export class DeleteAccountComponent {
	feedback: object | null;
	deleteAccountForm: FormGroup;
	@Output() rollback: EventEmitter<void> = new EventEmitter();

	constructor(
		private userService: UserService,
	) {
		this.deleteAccountForm = new FormGroup({
			password: new FormControl('', [Validators.required, passwordValidator]),
		})
	}

	get password() {
		return this.deleteAccountForm.get('password');
	}

	onDeleteSubmit() {
		this.userService.userDelete(this.deleteAccountForm.value).subscribe({
			next: (res) => this.feedback = { status: true, message: 'Un e-mail contenant un lien de confirmation vient de vous être envoyé.'},
			error: (err) => this.feedback =  { status: false, message: 'Erreur lors de la demande de suppression de votre compte.'},
			complete: () => setTimeout(() => {
				this.feedback = null;
				this.goBack();
			}, 5000),
		})
	}

	goBack() {
		this.rollback.emit();
	}
}
