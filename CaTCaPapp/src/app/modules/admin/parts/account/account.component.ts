import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { UserService } from "src/app/services/user/user.service";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

@Component({
	selector: "app-account",
	templateUrl: "./account.component.html",
	styleUrls: ["./account.component.scss"]
})
export class AccountComponent implements OnInit {
	faCheck = faCheck as IconProp;
	feedback: object | null;
	@Input() user: object;
	firstnameForm: FormGroup;
	lastnameForm: FormGroup;
	emailForm: FormGroup;
	typeForm: FormGroup;
	agreementForm: FormGroup;

	deleteAccount: boolean = false;
	passwordForm: boolean = false;

	constructor(
		private userService: UserService,
	) {
		this.firstnameForm = new FormGroup({
			firstname: new FormControl("", [Validators.required, Validators.minLength(3), Validators.pattern("^(?:[A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ][A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ-]*(?:\\s|-|$))+$")]),
		})
		this.lastnameForm = new FormGroup({
			lastname: new FormControl("", [Validators.required, Validators.minLength(3), Validators.pattern("^(?:[A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ][A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ-]*(?:\\s|-|$))+$")]),
		})
		this.emailForm = new FormGroup({
			email: new FormControl("", [Validators.required, Validators.email]),
		})
		this.typeForm = new FormGroup({
			type: new FormControl("", [Validators.required]),
		})
		this.agreementForm = new FormGroup({
			agreement: new FormControl(),
		})
	}
	
	get firstname() {
		return this.firstnameForm.get("firstname");
	}

	get lastname() {
		return this.lastnameForm.get("lastname");
	}

	get email() {
		return this.emailForm.get("email");
	}

	get type() {
		return this.typeForm.get("type");
	}

	get agreement() {
		return this.agreementForm.get("agreement");
	}

	capitalizeFirstLetters(formGroup: FormGroup, formControlName: string): void {
		const inputValue = formGroup.get(formControlName).value;
		const words = inputValue.split("-");

		const capitalizedWords = words.map(word => {
			if (word.length > 0) {
				return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
			} else {
				return "";
			}
		});

		formGroup.get(formControlName).setValue(capitalizedWords.join("-"));
	}

	updateUser(formGroup: FormGroup): void {
		switch (formGroup) {
			case this.firstnameForm:
				this.capitalizeFirstLetters(formGroup, "firstname");
				break;
			case this.lastnameForm:
				this.capitalizeFirstLetters(formGroup, "lastname");
				break;
			case this.typeForm:
				this.typeForm.markAsUntouched();
				this.typeForm.markAsPristine();
				break;
			default:
				break;
		};
		this.userService.userUpdate(formGroup.value).subscribe({
			next: (res) => this.feedback = { status: true, message: "Informations mises à jour."},
			error: (err) => this.feedback = { status: false, message: "Erreur lors de la mise à jour de vos informations."}
		});
		setTimeout(() => this.feedback = null, 5000);
	}

	setPasswordForm() {
		this.passwordForm = !this.passwordForm;
	}

	setDeleteAccount() {
		this.deleteAccount = !this.deleteAccount;
	}

	ngOnInit(): void {
		this.firstnameForm.patchValue({
			firstname: this.user["firstname"],
		});
		this.firstnameForm.markAsUntouched();
		this.firstnameForm.markAsPristine();
		this.lastnameForm.patchValue({
			lastname: this.user["lastname"],
		});
		this.lastnameForm.markAsUntouched();
		this.lastnameForm.markAsPristine();
		this.emailForm.patchValue({
			email: this.user["email"],
		});
		this.emailForm.markAsUntouched();
		this.emailForm.markAsPristine();
		this.typeForm.patchValue({
			type: this.user["type"],
		});
		this.typeForm.markAsUntouched();
		this.typeForm.markAsPristine();
		this.agreementForm.patchValue({
			agreement: this.user["mail_agreement"]
		});
	}
}
