import { Component } from '@angular/core';
import { ContentService } from 'src/app/services/content/content.service';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class ContentComponent {
  contentForm: FormGroup;
  HP: object[] = [];
  LLT: object[] = [];
  HLT: object[] = [];
  DF: object[] = [];
  DB: object[] = [];
  MB: object[] = [];
  RGPD: object[] = [];
  LN: object[] = [];
  NEWS: object[] = [];

  constructor(private contentService: ContentService) {
    this.contentForm = new FormGroup({
      content: new FormControl(null, [Validators.required]),
      type: new FormControl(null, [Validators.required]),
    });

    this.contentForm.get('content').valueChanges.subscribe((newValue) => {
      this.contentForm.patchValue({
        type: newValue['type'],
      });
    });

    this.contentForm.get('type').valueChanges.subscribe((newValue) => {
      this.contentForm.removeControl('text');
      this.contentForm.removeControl('video');
      this.contentForm.removeControl('order');
      this.contentForm.removeControl('title');
      this.contentForm.removeControl('subTitle');
      this.contentForm.removeControl('img');
      this.contentForm.removeControl('imgAlt');
      this.contentForm.removeControl('date');
    
      if (newValue === 'HP') {
        this.contentForm.addControl(
          'title',
          new FormControl(null, [
            Validators.required,
            Validators.minLength(10),
            Validators.maxLength(50),
          ])
        );
        this.contentForm.addControl(
          'subTitle',
          new FormControl(null, [
            Validators.minLength(20),
            Validators.maxLength(100),
          ])
        );
      } else if (newValue === 'RGPD' || newValue === 'LN') {
        this.contentForm.addControl(
          'title',
          new FormControl(null, [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(100),
          ])
        )
        this.contentForm.addControl(
          'text',
          new FormControl(null, [
            Validators.required,
            Validators.maxLength(1000),
          ])
        )
        this.contentForm.addControl(
          'order',
          new FormControl(null, [Validators.required, Validators.min(0)])
        )
      } else if (newValue === 'NEWS') {
        this.contentForm.addControl(
          'title',
          new FormControl(null, [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(100)
          ])
        );
        this.contentForm.addControl(
          'text',
          new FormControl(null, [
            Validators.required,
            Validators.minLength(20),
            Validators.maxLength(1000)
          ])
        );
        this.contentForm.addControl(
          'date',
          new FormControl(null, [
            Validators.required,
            this.minDateValidator()
          ])
        );
        this.contentForm.addControl(
          'img',
          new FormControl(null)
        );
        this.contentForm.addControl(
          'imgAlt',
          new FormControl(null)
        )
      } else {
        this.contentForm.addControl(
          'text',
          new FormControl(null, [
            Validators.required,
            Validators.maxLength(1000),
          ])
        );
        this.contentForm.addControl('video', new FormControl(null));
        this.contentForm.addControl(
          'order',
          new FormControl(null, [Validators.required, Validators.min(0)])
        );
      }

      if (this.contentForm.get('content').value !== 'new') {
        const content = this.contentForm.get('content').value;
        if (newValue === 'HP') {
          this.contentForm.patchValue({
            title: content['title'],
            subTitle: content['subTitle'],
          });
        } else if (newValue === 'RGPD' || newValue === 'LN') {
          this.contentForm.patchValue({
            title: content['title'],
            text: content['text'],
            order: content['order'],
          })
        } else if (newValue === 'NEWS') {
          this.contentForm.patchValue({
            title: content['title'],
            date: content['date'],
            text: content['text'],
            img: content['img'],
            imgAlt: content['img_alt']
          })
        } else {
          this.contentForm.patchValue({
            text: content['text'],
            video: content['video'],
            order: content['order'],
          });
        }
      }
    });

    this.getContents();
  }

  minDateValidator(): ValidatorFn {
    return (control: FormControl): { [key: string]: any } | null => {
      const selectedDate = new Date(control.value);
      const currentDate = new Date();
      currentDate.setHours(0, 0, 0, 0);
      if (selectedDate < currentDate) {
        return { 'invalidDate': true };
      }
      return null;
    }
  }

  getContents(): void {
    this.contentService.getContent('all').subscribe({
      next: (res) => {
        this.HP = [];
        this.NEWS = [];
        this.LLT = [];
        this.HLT = [];
        this.DF = [];
        this.DB = [];
        this.MB = [];
        this.RGPD = [];
        this.LN = [];
        res['data'].forEach((el) => {
          switch (el['type']) {
            case 'HP':
              this.HP.push(el);
              break;
            case 'NEWS':
              this.NEWS.push(el);
              break;
            case 'LLT':
              this.LLT.push(el);
              break;
            case 'HLT':
              this.HLT.push(el);
              break;
            case 'DF':
              this.DF.push(el);
              break;
            case 'DB':
              this.DB.push(el);
              break;
            case 'MB':
              this.MB.push(el);
              break;
            case 'RGPD':
              this.RGPD.push(el);
              break;
            case 'LN':
              this.LN.push(el);
              break;
            default:
              break;
          }
        });
      },
      complete: () => {
        this.LLT.sort((a, b) => a['order'] - b['order']);
        this.HLT.sort((a, b) => a['order'] - b['order']);
        this.DF.sort((a, b) => a['order'] - b['order']);
        this.DB.sort((a, b) => a['order'] - b['order']);
        this.MB.sort((a, b) => a['order'] - b['order']);
        this.RGPD.sort((a, b) => a['order'] - b['order']);
        this.LN.sort((a, b) => a['order'] - b['order']);
      }
    });
  }

  onSubmit(): void {
    if (this.contentForm.get('content').value === 'new') {
      this.contentService.postContent(this.contentForm.value).subscribe({
        complete: () => {
          this.getContents();
          this.contentForm.reset();
        },
      });
    } else {
      this.contentService
        .updateContent(
          this.contentForm.get('content').value['id'],
          this.contentForm.value
        )
        .subscribe({
          complete: () => {
            this.getContents();
            this.contentForm.reset();
          },
        });
    }
  }

  deleteContent(): void {
    this.contentService
      .deleteContent(this.contentForm.get('content').value['id'])
      .subscribe({
        complete: () => {
          this.getContents();
          this.contentForm.reset();
        },
      });
  }
}
