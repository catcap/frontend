import { Component, OnInit } from '@angular/core';
import { GroupService } from 'src/app/services/group/group.service';
import { faCrown, faEdit, faLock, faLockOpen, faMagnifyingGlass, faPlus, faRightToBracket, } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';

@Component({
	selector: 'app-groups',
	templateUrl: './groups.component.html',
	styleUrls: ['./groups.component.scss']
})
export class GroupsComponent {
	faCrown = faCrown as IconProp;
	faEdit = faEdit as IconProp;
	faLock = faLock as IconProp;
	faLockOpen = faLockOpen as IconProp;
	faPlus = faPlus as IconProp;
	faRightToBracket = faRightToBracket as IconProp;
	groups: object[] = [];
	public_groups: object[] = [];
	selectedGroup: object | null = null;
	state: string = 'groupList';

	constructor(
		private groupService: GroupService,
	) { }

	joinGroup(groupId: string): void {
		this.groupService.updategroup(groupId).subscribe({
			complete: () => this.ngOnInit(),
		})
	}

	toggleNewGroup(status: boolean = false): void {
		this.state = this.state === 'newGroup' ? 'groupList' : 'newGroup';
		if (status) this.ngOnInit()
	}

	toggleGroupDetails(del: boolean = false, group: object | null = null): void {
		if (del) {
			this.ngOnInit();
		}
		this.selectedGroup = this.selectedGroup === null ? group : null;
		this.state = this.state === 'groupDetails' ? 'groupList' : 'groupDetails';
	}

	ngOnInit(): void {
		this.groupService.getGroupList().subscribe({
			next: (res) => {
				this.groups = res['groups'];
				this.public_groups = res['public_groups'];
			},
		})
	}
}
