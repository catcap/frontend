import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faCheck, faLock, faLockOpen, faTrash, faX } from '@fortawesome/free-solid-svg-icons';
import { AdminService } from 'src/app/services/admin/admin.service';
import { endDateValidator } from 'src/app/validators/endDate.validator';
import { startDateValidator } from 'src/app/validators/startDate.validator';
import * as Papa from 'papaparse';
import * as FileSaver from "file-saver";

@Component({
	selector: 'app-back-office',
	templateUrl: './back-office.component.html',
	styleUrls: ['./back-office.component.scss']
})
export class BackOfficeComponent implements OnInit {
	error: boolean = false;
	notifForm: FormGroup;
	groups: object[] = [];
	servicing: boolean = false;
	user: object;
	users: object[] = [];
	
	faCheck = faCheck as IconProp;
	faLock = faLock as IconProp;
	faLockOpen = faLockOpen as IconProp;
	faTrash = faTrash as IconProp;
	faX = faX as IconProp;

	constructor(
		private adminService: AdminService,
	) {
		this.notifForm = new FormGroup({
			start: new FormControl('', [Validators.required, startDateValidator]),
			end: new FormControl('', [Validators.required]),
		}, {
			validators: endDateValidator,
		})
	}

	get start() {
		return this.notifForm.get("start");
	}

	get end () {
		return this.notifForm.get("end");
	}

	toggleDeleteUser(user: object | null = null): void {
		this.user = user;
	}

	deleteUser(user_id: string): void {
		this.adminService.deleteUser(user_id).subscribe({
			error: (error) => this.error = true,
			complete: () => {
				this.users.splice(this.users.findIndex(user => user["id"] === user_id), 0);
				this.toggleDeleteUser();
				this.ngOnInit();
			},
		})
	}
	
	toggleServicing(): void {
		this.servicing = !this.servicing;
	}

	notifyServicing(): void {
		this.adminService.sendNotification(this.notifForm.value).subscribe({
			complete: () => this.toggleServicing(),
		})
	}

	downloadUsersList(): void {
		const csv = Papa.unparse(this.users);
		const blob = new Blob([csv], { type: 'text/csv' });
		const currentDate = new Date();
		const year = currentDate.getFullYear();
		const month = ("0" + (currentDate.getMonth() +1)).slice(-2);
		const day = ("0" + currentDate.getDate()).slice(-2);
		FileSaver.saveAs(blob, `${year}_${month}_${day}_catcap_users.csv`)	
	}

	ngOnInit(): void {
		this.adminService.getUsers().subscribe({
			next: (res) => {
				res.forEach(user => {
					if (user["last_login"] !== null) {
						const date = new Date(user["last_login"]);
						user["last_login"] = `${date.toLocaleDateString()}`;
					} else {
						user["last_login"] = "Ø"
					}
				})
				this.users = res;
			},
		});
		this.adminService.getGroups().subscribe({
			next: (res) => this.groups = res,
		})
	}
}
