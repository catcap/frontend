import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
	selector: 'app-admin',
	templateUrl: './admin.component.html',
	styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
	state: string = 'general';
	user: object;
	isAdmin: boolean = false;
	constructor(
		private userService: UserService,
	) { }
	
	changeState(state: string): void {
		this.state = state;
	} 

	ngOnInit(): void {
		this.userService.me().subscribe({
			next: (res) => {
				this.user = res;
				this.isAdmin = res["is_superuser"];
			},
		})
	}
}
