import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateGroupsComponent } from './parts/create-groups/create-groups.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProxymaDeleterModule } from '../shared/proxyma-deleter/proxyma-deleter.module';
import { GroupSelectorModule } from '../shared/group-selector/group-selector.module';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { GroupsComponent } from './parts/groups/groups.component';
import { AccountComponent } from './parts/account/account.component';
import { DeleteAccountComponent } from './parts/delete-account/delete-account.component';
import { GroupDetailsComponent } from './parts/group-details/group-details.component';
import { PasswordComponent } from './parts/password/password.component';
import { ProjectsComponent } from './parts/projects/projects.component';
import { BackOfficeComponent } from './parts/back-office/back-office.component';
import { ContentComponent } from './parts/content/content.component';



@NgModule({
  declarations: [
    AdminComponent,
    AccountComponent,
    CreateGroupsComponent,
    DeleteAccountComponent,
    GroupDetailsComponent,
    GroupsComponent,
    PasswordComponent,
    ProjectsComponent,
    BackOfficeComponent,
    ContentComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    ProxymaDeleterModule,
    GroupSelectorModule,
    RouterModule.forChild([{path: '', component: AdminComponent}])
  ],
  exports: [AdminComponent],
  
})
export class AdminModule { }
