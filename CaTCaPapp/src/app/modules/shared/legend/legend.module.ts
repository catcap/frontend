import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LegendComponent } from './legend.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';



@NgModule({
  declarations: [LegendComponent],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [LegendComponent],
})
export class LegendModule { }
