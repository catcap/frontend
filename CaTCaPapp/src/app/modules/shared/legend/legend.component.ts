import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { faAngleUp, faAngleDown, faCircle } from '@fortawesome/free-solid-svg-icons'
import { IconProp } from '@fortawesome/fontawesome-svg-core';

@Component({
	selector: 'app-legend',
	templateUrl: './legend.component.html',
	styleUrls: ['./legend.component.scss']
})
export class LegendComponent implements OnChanges {
	@Input() mapLoaded: boolean = false;
	faAngle = faAngleDown as IconProp;
	faCircle = faCircle as IconProp;
	legend: boolean = false;

	toggleLegend(): void {
		this.faAngle = this.legend ? faAngleDown as IconProp : faAngleUp as IconProp;
		this.legend = !this.legend;
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["mapLoaded"]) {
			const newValue = changes["mapLoaded"].currentValue;
			if (!newValue) this.legend = false;
		}
	}
}
