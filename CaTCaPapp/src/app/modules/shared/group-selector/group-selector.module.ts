import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupSelectorComponent } from './group-selector.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';



@NgModule({
  declarations: [GroupSelectorComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ReactiveFormsModule,
  ],
  exports: [GroupSelectorComponent],
  
})
export class GroupSelectorModule { }
