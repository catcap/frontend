import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GroupService } from 'src/app/services/group/group.service';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { UserService } from 'src/app/services/user/user.service';
import { ProxymaService } from 'src/app/services/proxyma/proxyma.service';
import { DataService } from 'src/app/services/data/data.service';

@Component({
	selector: 'app-group-selector',
	templateUrl: './group-selector.component.html',
	styleUrls: ['./group-selector.component.scss']
})
export class GroupSelectorComponent implements OnInit {
	faCheck = faCheck as IconProp;
	groups: object[] = [];
	groupForm: FormGroup;
	activeGroupId: String = '';
	noGroup: boolean = false;

	constructor(
		private dataService: DataService,
		private groupService: GroupService,
		private proxymaService: ProxymaService,
		private userService: UserService,
	) {
		this.groupForm = new FormGroup({
			workGroup: new FormControl('', [Validators.required])
		})
	}

	get workGroup(): any {
		return this.groupForm.get('workGroup');
	}

	updateWorkGroup() {
		this.userService.userUpdate(this.groupForm.value).subscribe({
			next: (res) => {
				this.ngOnInit();
			},
			complete: () => {
				this.proxymaService.reload();
				this.dataService.reload();
			},
		})
	}

	closeNoGroup(): void {
		this.noGroup = false;
	}

	ngOnInit(): void {
		this.groupService.getGroupList().subscribe((res) => {
			this.groups = res['groups'];
			if (res['groups'].length <= 0) this.noGroup = true;
			for (let group of res['groups']) {
				if (group['active']) {
					this.groupForm.patchValue({workGroup: String(group['id'])});
					this.activeGroupId = String(group['id']);
				}
			}
		})
	}
}
