import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProxymaDeleterComponent } from './proxyma-deleter.component';



@NgModule({
  declarations: [ProxymaDeleterComponent],
  imports: [
    CommonModule
  ],
  exports: [ProxymaDeleterComponent],
  
})
export class ProxymaDeleterModule { }
