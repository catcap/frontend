import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProxymaDeleterComponent } from './proxyma-deleter.component';

describe('ProxymaDeleterComponent', () => {
  let component: ProxymaDeleterComponent;
  let fixture: ComponentFixture<ProxymaDeleterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProxymaDeleterComponent]
    });
    fixture = TestBed.createComponent(ProxymaDeleterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
