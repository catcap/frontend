import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FileUploadService } from 'src/app/services/file_upload/file-upload.service';

@Component({
	selector: 'app-proxyma-deleter',
	templateUrl: './proxyma-deleter.component.html',
	styleUrls: ['./proxyma-deleter.component.scss']
})
export class ProxymaDeleterComponent {
	@Input() proxyma_corpus_id: string;
	feedback: string = null;
	@Output() rollBack: EventEmitter<boolean> = new EventEmitter();

	constructor(
		private fileUploadService: FileUploadService,
	) { }

	deleteToggler(): void {
		this.rollBack.emit(false);
	}

	delete() {
		this.fileUploadService.deleteProxyma(this.proxyma_corpus_id).subscribe({
			error: () => this.feedback = "error",
			complete: () => this.rollBack.emit(true),
		});
	}
}
