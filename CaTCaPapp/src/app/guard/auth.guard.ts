import { Injectable } from '@angular/core';
import { Observable, catchError, firstValueFrom, lastValueFrom, map, of, switchMap } from 'rxjs';
import { Router } from '@angular/router';
import { JwtService } from '../services/jwt/jwt.service';
import { MapsComponent } from '../modules/maps/maps.component';
import { AlertService } from '../services/alert/alert.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
    constructor(
        private alertService: AlertService,
        private jwtService: JwtService,
        private router: Router
    ) {}

	canActivate(): Observable<boolean> {
		return this.jwtService.verifyToken().pipe(
            switchMap(validAccessToken => {
                if (validAccessToken) {
                    return of(true);
                } else {
                    return of(false);
                }
            }),
            catchError(() => {
                    return this.jwtService.refreshToken().pipe(
                        switchMap(() => this.jwtService.verifyToken()),
                        map(newValidAccessToken => {
                            if (newValidAccessToken) {
                                return true;
                            } else {
                                this.router.navigate(['/login']);
                                return false;
                            }
                        }),
                        catchError(() => {
                            this.router.navigate(['/login']);
                            return of(false);
                        })
                    );
            })
		);
	}

    async canDeactivate(component: MapsComponent): Promise<boolean> {
        if (component.hasUnsavedChanges()) {
            this.alertService.show(true);
            const response = await firstValueFrom(this.alertService.userResponse$);
            return response;
        }
        return true;
    }
}
