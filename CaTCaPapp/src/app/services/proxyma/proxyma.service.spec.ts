import { TestBed } from '@angular/core/testing';

import { ProxymaService } from './proxyma.service';

describe('ProxymaService', () => {
  let service: ProxymaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProxymaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
