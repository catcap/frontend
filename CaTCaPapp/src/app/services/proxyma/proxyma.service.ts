import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable, catchError, retry, throwError } from 'rxjs';
import { API } from '../../defs/enums/api';
import { environment } from '../../../environments/environment';
import { JwtService } from '../jwt/jwt.service';
import { FormControl } from '@angular/forms';

@Injectable({
	providedIn: 'root'
})
export class ProxymaService {
	reloadProxyma: EventEmitter<void> = new EventEmitter();

	constructor(
		private http: HttpClient,
		private jwtService: JwtService,
	) { }

	getProxymaList(): Observable<String[]> {
		this.jwtService.refreshToken().subscribe();
		return this.http.get<String[]>(`${environment.API}${API.Proxyma_List}`)
			.pipe(
				catchError(this.handleError)
			)
	}

	reload() {
		this.reloadProxyma.emit();
	}

	proxymaLoader(proxyma_id: string): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		return this.http.get<any>(`${environment.API}${API.Select_Proxyma}/${proxyma_id}`)
			.pipe(
				catchError(this.handleError)
			)
	}

	updateProxymaTitle(proxyma_id: string, proxyma_title: FormControl): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		return this.http.put<any>(`${environment.API}${API.Select_Proxyma}/${proxyma_id}`, proxyma_title)
			.pipe(
				catchError(this.handleError)
			)
	}


	private handleError(error: HttpErrorResponse) {
		return throwError(() => error["error"]);
	}
}
