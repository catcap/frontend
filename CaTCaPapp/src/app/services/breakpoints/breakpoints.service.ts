import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BreakpointsService {
  windowWidth: number = window.innerWidth;
  type: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null);

  constructor() {
    this.checkBreakpoints();
  }

  checkBreakpoints(): void {
    this.windowWidth = window.innerWidth;
    if (this.windowWidth < 640) this.type.next("xs");
    else if (this.windowWidth >= 640 && this.windowWidth < 1024) this.type.next("md");
    else if (this.windowWidth >= 1024 && this.windowWidth < 1200) this.type.next("lg");
    else if (this.windowWidth >= 1200) this.type.next("xl")
    else this.type.next(null);
  }

  getTypeObservable(): Observable<string | null> {
    return this.type.asObservable();
  }
}
