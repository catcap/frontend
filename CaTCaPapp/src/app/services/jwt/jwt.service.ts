import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API } from '../../defs/enums/api';
import { environment } from '../../../environments/environment';

@Injectable({
  	providedIn: 'root',
})
export class JwtService {
  	constructor(
		private http: HttpClient,
	) { }

	verifyToken(): Observable<any> {
		return this.http.get<any>(`${environment.API}${API.Token_verify}`);
	}

	refreshToken(): Observable<any> {
		return this.http.get<any>(`${environment.API}${API.Token_refresh}`);
	}
}
