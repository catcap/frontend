import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtService } from '../jwt/jwt.service';
import { environment } from 'src/environments/environment';
import { API } from 'src/app/defs/enums/api';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ContentService {
  constructor(private http: HttpClient, private jwtService: JwtService) {}

  getContent(type: string): Observable<object[]> {
    return this.http
      .get<object[]>(`${environment.API}${API.GetContent}/${type}`)
      .pipe(catchError(this.handleError));
  }

  postContent(data: object): Observable<any>{
    return this.http
      .post<any>(`${environment.API}${API.HandleContent}`, data)
      .pipe(catchError(this.handleError));
  }

  updateContent(contentId: string, data: object): Observable<any>{
    return this.http
      .put<any>(`${environment.API}${API.HandleContent}/${contentId}`, data)
      .pipe(catchError(this.handleError));
  }

  deleteContent(contentId: string): Observable<any>{
    return this.http
      .delete<any>(`${environment.API}${API.HandleContent}/${contentId}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(() => error['error']);
  }
}
