import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AlertService {
	private showAlertSource = new Subject<boolean>();
	showAlert$ = this.showAlertSource.asObservable();

	private userResponseSource = new Subject<boolean>();
	userResponse$ = this.userResponseSource.asObservable();

	show(message: boolean) {
		this.showAlertSource.next(message);
	}

	getUserResponse(response: boolean) {
		this.userResponseSource.next(response);
	}
}
