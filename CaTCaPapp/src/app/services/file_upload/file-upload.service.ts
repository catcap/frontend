import {
  HttpClient,
  HttpErrorResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { API } from 'src/app/defs/enums/api';
import { environment } from 'src/environments/environment';
import { JwtService } from '../jwt/jwt.service';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  constructor(private http: HttpClient, private jwtService: JwtService) {}

  uploadFile(file: object[]): Observable<ArrayBuffer> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .post<any>(`${environment.API}${API.Post_Data}`, {file: file})
      .pipe(catchError(this.handleError));
  }

  updateProxyma(
    proxyma_id: string,
    file: object[]
  ): Observable<ArrayBuffer> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .put<any>(`${environment.API}${API.Post_Data}/${proxyma_id}`, {file: file})
      .pipe(catchError(this.handleError));
  }

  deleteProxyma(proxyma_id: string): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .delete(`${environment.API}${API.Delete_Proxyma}/${proxyma_id}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(() => error['error']);
  }
}
