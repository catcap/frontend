import { EventEmitter, Injectable } from '@angular/core';
import { WebSocketSubject } from 'rxjs/webSocket';
import { map } from 'rxjs/operators';

import { API } from 'src/app/defs/enums/api';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class WebsocketService {
	notificationEventEmitter: EventEmitter<boolean> = new EventEmitter();
	private isConnected: boolean = false;
	private subject: WebSocketSubject<string>;

	constructor() { }

	public connectWebSocket(username: string): WebSocketSubject<string> {
		if (!this.isConnected) {			
			this.connect(`${environment.WS}${API.Notifications}/${username}/`);
			this.isConnected = true;
		}
		return this.subject;
	}

	private connect(url: string): WebSocketSubject<string> {
		if (!this.subject || this.subject.closed) {
			this.subject = this.create(url);
		}
		return this.subject;
	}

	private create(url: string): WebSocketSubject<string> {
		return new WebSocketSubject({
			url,
			serializer: msg => JSON.stringify(msg),
			deserializer: (e: MessageEvent) => JSON.parse(e.data),
		});
	}

	public disconnectWebSocket(): void {
		if (this.subject) {
			this.subject.complete();
			this.subject = null;
		}
	}

	public getMessages(): Observable<string> {
		if (this.subject) {
			return this.subject.pipe(
				map((message: string) => {
					this.notificationEventEmitter.emit(true);
					return message;
				})
			);
		} else {
			return new Observable();
		}
	}

	public sendData(jsonData): void {
		if (this.subject) {
			this.subject.next(jsonData)
		}
	}
}
