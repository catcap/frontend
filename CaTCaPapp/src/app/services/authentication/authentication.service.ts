import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from 'src/app/defs/enums/api';
import { environment } from 'src/environments/environment';
import { JwtService } from '../jwt/jwt.service';

@Injectable({
	providedIn: 'root',
})
export class AuthenticationService {
	constructor(
		private http: HttpClient,
		private jwtService: JwtService,
	) { }


	register(userForm: FormGroup): Observable<any> {
		return this.http.post<any>(`${environment.API}${API.Register}`, userForm).pipe(
			catchError(this.handleError)
		);
	}

	confirmRegister(uidb64: string, token: string): Observable<any> {
		return this.http.get<any>(`${environment.API}${API.Confirm_register}/${uidb64}/${token}`).pipe(
			catchError(this.handleError)
		);
	}

	forgottenPassword(email: string): Observable<any> {
		return this.http.post<any>(`${environment.API}${API.Forgotten_Password}`, { email: email }).pipe(
			catchError(this.handleError)
		);
	}

	resetPassword(uidb64: string, token: string, passwd: string): Observable<any> {
		return this.http.post<any>(`${environment.API}${API.Reset_Password}/${uidb64}/${token}`, { password: passwd }).pipe(
			catchError(this.handleError),
		);
	}

	changePassword(passwordForm: FormGroup): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		return this.http.post<any>(`${environment.API}${API.Change_Password}`, passwordForm).pipe(
			catchError(this.handleError),
		)
	}

	login(loginForm: FormGroup): Observable<any> {
		return this.http.post<any>(`${environment.API}${API.Login}`, loginForm)
			.pipe(
				catchError(this.handleError)
			);
		}

	logout(): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		return this.http.get<any>(`${environment.API}${API.Logout}`).pipe(
			catchError(this.handleError)
		);
	}

	private handleError(error: HttpErrorResponse) {
		return throwError(() => error["error"]);
	}
}
