import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { JwtService } from '../jwt/jwt.service';
import { Observable, catchError, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API } from 'src/app/defs/enums/api';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private http: HttpClient,
    private jwtService: JwtService,
  ) { }

  getUsers(): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http.get<any>(`${environment.API}${API.AdminUsers}`)
      .pipe(
        catchError(this.handleError),
      );
  }

  deleteUser(user_id: string): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http.delete<any>(`${environment.API}${API.AdminUsers}/${user_id}`)
      .pipe(
        catchError(this.handleError),
      );
  }

  getGroups(): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http.get<any>(`${environment.API}${API.AdminGroups}`)
      .pipe(
        catchError(this.handleError),
      );
  }

  sendNotification(notifForm: FormControl): Observable<any> {
		this.jwtService.refreshToken().subscribe();
    return this.http.post<any>(`${environment.API}${API.AdminNotif}`, notifForm)
      .pipe(
        catchError(this.handleError),
      );
  }

	private handleError(error: HttpErrorResponse) {
		return throwError(() => error["error"]);
	}
}
