import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtService } from '../jwt/jwt.service';
import { Observable, catchError, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API } from 'src/app/defs/enums/api';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(
    private http: HttpClient,
    private jwtService: JwtService,
  ) { }

	getGroupList(): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		return this.http.get<any>(`${environment.API}${API.Groups}`).pipe(
			catchError(this.handleError),
		)
	}

	addgroup(groupForm: FormGroup, users: string[]): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		const group = {
			group_name: groupForm['group_name'],
			group_users: users,
			group_status: groupForm['group_status'],
		}
		return this.http.post<any>(`${environment.API}${API.Groups}`, group)
			.pipe(
				catchError(this.handleError),
			)
	}

	updategroup(groupUID: string, data: object = {}): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		return this.http.put(`${environment.API}${API.Groups}/${groupUID}`, data)
			.pipe(
				catchError(this.handleError),
			)
	}

	deleteSubgroup(subGroupUID: string): Observable<any> {
		this.jwtService.refreshToken().subscribe();
		return this.http.delete<any>(`${environment.API}${API.Groups}/${subGroupUID}`)
			.pipe(
				catchError(this.handleError),
			);
	}

  private handleError(error: HttpErrorResponse) {
		return throwError(() => error["error"]);
	}
}
