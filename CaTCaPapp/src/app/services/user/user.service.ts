import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable, catchError, map, of, throwError } from 'rxjs';
import { API } from 'src/app/defs/enums/api';
import { environment } from 'src/environments/environment';
import { FormControl } from '@angular/forms';
import { JwtService } from '../jwt/jwt.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user$: EventEmitter<object | null> = new EventEmitter();
  isLoggedIn: boolean = false;

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  me(): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http.get<any>(`${environment.API}${API.Me}`).pipe(
      map((res) => {
        if (res) {
          this.user$.emit(res);
          this.isLoggedIn = true;
        } else {
          this.user$.emit(null);
          this.isLoggedIn = false;
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  isLogged(): Observable<boolean> {
    return this.me().pipe(
      map((res) => res),
      catchError(() => of(false))
    );
  }

  noMe(): void {
    this.user$.emit(null);
    this.isLoggedIn = false;
  }

  userUpdate(userForm: FormControl): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .post<any>(`${environment.API}${API.Me}`, userForm)
      .pipe(catchError(this.handleError));
  }

  userDelete(deleteForm: FormControl): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .post<any>(`${environment.API}${API.Delete_Account}`, deleteForm)
      .pipe(catchError(this.handleError));
  }

  confirmDelete(uidb64: string, token: string): Observable<any> {
    return this.http
      .delete<any>(
        `${environment.API}${API.Confirm_Delete_Account}/${uidb64}/${token}`
      )
      .pipe(catchError(this.handleError));
  }

  contact(contactForm: FormControl): Observable<any> {
    return this.http
      .post<any>(`${environment.API}${API.Contact}`, contactForm)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(() => error['error']);
  }
}
