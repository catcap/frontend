import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { JwtService } from '../jwt/jwt.service';
import { environment } from 'src/environments/environment';
import { API } from 'src/app/defs/enums/api';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  reloadCorpus: EventEmitter<void> = new EventEmitter();
  refreshData: EventEmitter<void> = new EventEmitter();

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  reload() {
    this.reloadCorpus.emit();
  }

  refresh() {
    this.refreshData.emit();
  }

  postRawData(data: FormData): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .post<any>(`${environment.API}${API.PostRawData}`, data)
      .pipe(catchError(this.handleError));
  }

  getCorpusNames(): Observable<String[]> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .get<String[]>(`${environment.API}${API.GetCorpusNames}`)
      .pipe(catchError(this.handleError));
  }

  getData(corpusId: String): Observable<Object[]> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .get<Object[]>(`${environment.API}${API.GetData}/${corpusId}`)
      .pipe(catchError(this.handleError));
  }

  getRawData(corpusId: String): Observable<Object[]> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .get<Object[]>(`${environment.API}${API.GetRawData}/${corpusId}`)
      .pipe(catchError(this.handleError));
  }

  getPreMap(corpusTitle: String, corpusId: String): Observable<Object[]> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .get<Object[]>(`${environment.API}${API.GetPreMap}/${corpusTitle}/${corpusId}`)
      .pipe(catchError(this.handleError));
  }

  validateData(endPoint: string, formData: FormData): Observable<ArrayBuffer> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .post<any>(`${environment.API}${API.ValidateActivity}/${endPoint}`, formData)
      .pipe(catchError(this.handleError));
  }

  deleteCorpus(corpusId: string): Observable<any> {
    this.jwtService.refreshToken().subscribe();
    return this.http
      .delete<any>(`${environment.API}${API.DeleteCorpus}/${corpusId}`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(() => error['error']);
  }
}
