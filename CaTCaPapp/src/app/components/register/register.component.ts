import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ConfirmPasswordValidator } from '../../validators/confirm-password.validator';
import { passwordValidator } from '../../validators/password.validator';
import { Router } from '@angular/router';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
	groups: String[];
	userForm: FormGroup;
	newGroup: boolean = false;
	successMessage: boolean = false;
	failMessage: boolean = false;
	timer: number = 5;

	constructor(
		private authService: AuthenticationService,
		private router: Router,
	) {
		this.userForm = new FormGroup({
			firstname: new FormControl('', [Validators.required, Validators.minLength(3), Validators.pattern('^(?:[A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ][A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ-]*(?:\\s|-|$))+$')]),
			lastname: new FormControl('', [Validators.required, Validators.minLength(3), Validators.pattern('^(?:[A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ][A-ZÀÂÄÆÇÉÈÊËÎÏÔŒÙÛÜŸa-zàâäæçéèêëîïôœùûüÿ-]*(?:\\s|-|$))+$')]),
			password: new FormControl('', [Validators.required, passwordValidator]),
			confirmPassword: new FormControl('', [Validators.required]),
			email: new FormControl('', [Validators.required, Validators.email]),
			type: new FormControl('', [Validators.required]),
			legalNotice: new FormControl(null, [Validators.required])
		}, {
			validators: ConfirmPasswordValidator
		})
	}

	cleanGroupName(): void {
		this.newGroup = !this.newGroup;
		this.userForm.get('group_name').markAsUntouched();
	}

	get firstname() {
		return this.userForm.get('firstname');
	}

	get lastname() {
		return this.userForm.get('lastname');
	}

	get password() {
		return this.userForm.get('password');
	}

	get confirmPassword() {
		return this.userForm.get('confirmPassword');
	}

	get email() {
		return this.userForm.get('email');
	}

	get type() {
		return this.userForm.get("type");
	}

	get legalNotice() {
		return this.userForm.get('legalNotice');
	}

	capitalizeFirstLetters(formControlName: string) {
		const inputValue = this.userForm.get(formControlName).value;
		const words = inputValue.split('-');

		const capitalizedWords = words.map(word => {
			if (word.length > 0) {
				return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
			} else {
				return '';
			}
		});

		this.userForm.get(formControlName).setValue(capitalizedWords.join('-'));
	}

	onSubmit() {
		this.capitalizeFirstLetters('firstname');
		this.capitalizeFirstLetters('lastname');

		this.authService.register(this.userForm.value).subscribe({

			next: () => this.successMessage = true,
			error: () => {
				this.failMessage = true
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.router.navigateByUrl('/');
					}
					this.timer--;
				}, 1000)
			},
			complete: () => {
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.router.navigateByUrl('/');
					}
					this.timer--;
				}, 1000)
			}
		})
	}
}
