import { Component, OnInit } from '@angular/core';
import { AddonService } from 'src/app/services/addon/addon.service';

@Component({
  selector: 'app-update-manifest',
  templateUrl: './update-manifest.component.html',
  styleUrl: './update-manifest.component.scss',
})
export class UpdateManifestComponent implements OnInit {
  constructor(private addonService: AddonService) {}

  ngOnInit(): void {
    this.addonService.getUpdateManifest().subscribe(data => this.downloadUpdateManifest(data, 'update.json'));
  }

  downloadUpdateManifest(data: any, filename: string): void {
    const json = JSON.stringify(data);
    const blob = new Blob([json], { type: 'application/json' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
    window.location.href = '/';
  }
}
