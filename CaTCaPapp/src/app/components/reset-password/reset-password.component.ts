import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { passwordValidator } from '../../validators/password.validator';
import { ConfirmPasswordValidator } from '../../validators/confirm-password.validator';

@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
	passwdForm: FormGroup;
	uidb64: string;
	token: string;
	successMessage: boolean = false;
	failMessage: boolean = false;
	timer: number = 5;

	constructor(
		private authService: AuthenticationService,
		private route: ActivatedRoute,
		private router: Router,
	) {
		this.passwdForm = new FormGroup({
			password: new FormControl('', [Validators.required, passwordValidator]),
			confirmPassword: new FormControl('', [Validators.required]),
		}, {
			validators: ConfirmPasswordValidator
		})
	}

	ngOnInit(): void {
		this.uidb64 = this.route.snapshot.paramMap.get('uidb64');
		this.token = this.route.snapshot.paramMap.get('token');
	}

	get password() {
		return this.passwdForm.get('password');
	}

	get confirmPassword() {
		return this.passwdForm.get('confirmPassword');
	}

	sumbit(): void {
		this.authService.resetPassword(this.uidb64, this.token, this.password.value).subscribe({
			next: () => this.successMessage = true,
			error: () => {
				this.failMessage = true
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.router.navigateByUrl('/');
					}
					this.timer--;
				}, 1000);
			},
			complete: () => {
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.router.navigateByUrl('/');
					}
					this.timer--;
				}, 1000);
			}
		})
	}
}
