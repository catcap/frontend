import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
	selector: 'app-confirm-delete-account',
	templateUrl: './confirm-delete-account.component.html',
	styleUrls: ['./confirm-delete-account.component.scss']
})
export class ConfirmDeleteAccountComponent implements OnInit, AfterViewInit {
	feedback: object|null;
	timer: number = 5;
	uidb64: string;
	token: string;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		private userService: UserService,
	) { }

	ngOnInit(): void {
		this.uidb64 = this.route.snapshot.paramMap.get('uidb64');
		this.token = this.route.snapshot.paramMap.get('token');
	}

	ngAfterViewInit(): void {
		this.userService.confirmDelete(this.uidb64, this.token).subscribe({
			next: () => this.feedback = { status: true, message: 'Compte supprimé' },
			error: () => {
				this.feedback = { status: false, message: 'Échec de la suppression du compte'}
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.authenticationService.logout().subscribe();
						this.router.navigateByUrl('/');
						
					}
					this.timer --;
				}, 1000)
			},
			complete: () => {
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.authenticationService.logout().subscribe();
						this.router.navigateByUrl('/');
						
					}
					this.timer --;
				}, 1000)
			}
		})
	}
}
