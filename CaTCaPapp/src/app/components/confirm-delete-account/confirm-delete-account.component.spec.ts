import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDeleteAccountComponent } from './confirm-delete-account.component';

describe('ConfirmDeleteAccountComponent', () => {
  let component: ConfirmDeleteAccountComponent;
  let fixture: ComponentFixture<ConfirmDeleteAccountComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmDeleteAccountComponent]
    });
    fixture = TestBed.createComponent(ConfirmDeleteAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
