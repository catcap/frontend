import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss',
})
export class ContactComponent {
  contactForm: FormGroup;
  successMessage: boolean = false;
  timer: number = 5;

  constructor(private userService: UserService, private router: Router) {
    this.contactForm = new FormGroup({
      from: new FormControl(null, [Validators.required, Validators.email]),
      subject: new FormControl(null, [Validators.required]),
      message: new FormControl(null, [Validators.required]),
      honeyPot: new FormControl(null),
    });
  }

  get from(): any {
    return this.contactForm.get('from');
  }

  get subject(): any {
    return this.contactForm.get('subject');
  }

  get message(): any {
    return this.contactForm.get('message');
  }

  get honeyPot(): any {
    return this.contactForm.get('honeyPot');
  }

  onSubmit() {
    if (!this.honeyPot.value) {
      this.userService.contact(this.contactForm.value).subscribe({
        next: () => (this.successMessage = true),
        complete: () => {
          const interval = setInterval(() => {
            if (this.timer === 0) {
              clearInterval(interval);
              this.router.navigate(['/']);
            }
            this.timer--;
          }, 1000);
        },
      });
    } else {
      this.successMessage = true;
      const interval = setInterval(() => {
        if (this.timer === 0) {
          clearInterval(interval);
          this.router.navigate(['/']);
        }
        this.timer--;
      }, 1000);
    }
  }
}
