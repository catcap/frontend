import { Component } from '@angular/core';
import { ContentService } from 'src/app/services/content/content.service';

@Component({
  selector: 'app-legal-notice',
  templateUrl: './legal-notice.component.html',
  styleUrl: './legal-notice.component.scss'
})
export class LegalNoticeComponent {
  contents: object[];

  constructor(private contentService: ContentService) {
    this.contentService.getContent('LN').subscribe({
      next: (res) => this.contents = res['data'],
      complete: () => this.contents.sort((a, b) => a['order'] - b['order'])
    })
  }
}
