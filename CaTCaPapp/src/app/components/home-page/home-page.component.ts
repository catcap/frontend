import { Component } from '@angular/core';
import { ContentService } from 'src/app/services/content/content.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {
  contents: object = {};
  news: object[] = [];

  constructor(private contentService: ContentService,) {
    this.contentService.getContent('HP').subscribe({
			next: (res) => this.contents = res['data'][0],
		});
    this.contentService.getContent('NEWS').subscribe({
      next: (res) => this.news = res['data'],
    });
  }

}
