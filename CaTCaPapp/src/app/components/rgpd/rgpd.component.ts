import { Component, OnInit } from '@angular/core';
import { ContentService } from 'src/app/services/content/content.service';

@Component({
  selector: 'app-rgpd',
  templateUrl: './rgpd.component.html',
  styleUrl: './rgpd.component.scss'
})
export class RGPDComponent implements OnInit {
  contents: object[] = [];

  constructor(private contentService: ContentService) {

  }

  ngOnInit(): void {
    this.contentService.getContent('RGPD').subscribe({
      next: (res) => this.contents = res['data'] ? res['data'] : [],
      complete: () => this.contents.sort((a, b) => a['order'] - b['order'])
    })
  }
}
