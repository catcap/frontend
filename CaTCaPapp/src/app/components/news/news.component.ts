import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrl: './news.component.scss',
})
export class NewsComponent implements AfterViewInit {
  @ViewChild('sliderWrapper') sliderWrapper: ElementRef;
  @ViewChildren('slide') allSlides: QueryList<ElementRef>;
  counter: number = 0;
  sliderWrapperLength: number;
  slideWidth: number;
  gap: string;

  @Input() news: object[] = [];

  faArrowLeft = faArrowLeft as IconProp;
  faArrowRight = faArrowRight as IconProp;

  constructor() { }

  ngAfterViewInit(): void {
    if (this.allSlides.length > 0) {
      this.sliderWrapperLength = this.news.length -1;
      this.slideWidth = this.allSlides.first.nativeElement.getBoundingClientRect().width;
    }
  }

  previous(): void {
    this.counter--;
    this.counter = this.counter < 0 ? this.sliderWrapperLength : this.counter;
    this.sliderWrapper.nativeElement.style.transition =
      this.counter < 0 ? 'left .6s unset' : 'left .6s linear';
    this.gap =
      this.counter < 0
        ? `${this.slideWidth * this.counter}px`
        : `${-this.slideWidth * this.counter}px`;
    this.sliderWrapper.nativeElement.style.left = this.gap;
  }

  next(): void {
    this.counter++;
    this.counter =
      this.counter > this.sliderWrapperLength ? 0 : this.counter;
    this.sliderWrapper.nativeElement.style.transition =
      this.counter > this.sliderWrapperLength
        ? 'left .6s unset'
        : 'left .6s linear';
    this.gap =
      this.counter > this.sliderWrapperLength
        ? '0'
        : `-${this.slideWidth * this.counter}px`;
    this.sliderWrapper.nativeElement.style.left = this.gap;
  }
}
