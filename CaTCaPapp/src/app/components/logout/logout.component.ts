import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { JwtService } from 'src/app/services/jwt/jwt.service';
import { UserService } from 'src/app/services/user/user.service';
import { WebsocketService } from 'src/app/services/websocket/websocket.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(
    private authService: AuthenticationService,
    private userService: UserService,
    private webSocketService: WebsocketService,
    private router: Router
  ) { }

  logout() {
    this.authService.logout().subscribe({
      complete: () => {
        this.userService.noMe();
        this.webSocketService.disconnectWebSocket();
      }
    });
    this.router.navigateByUrl('/');
  }
  
  ngOnInit(): void {
    this.logout();
  }

}
