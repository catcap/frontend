import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
	selector: 'app-forgotten-password',
	templateUrl: './forgotten-password.component.html',
	styleUrls: ['./forgotten-password.component.scss']
})
export class ForgottenPasswordComponent {
	forgotForm: FormGroup;
	successMessage: boolean = false;
	failMessage: boolean = false;
	timer: number = 5;

	constructor(
		private authService: AuthenticationService,
		private router: Router
	) {
		this.forgotForm = new FormGroup({
			email: new FormControl('', [Validators.email, Validators.required])
		})
	}

	get email() {
		return this.forgotForm.get('email');
	}

	submit(): void {
		this.authService.forgottenPassword(this.email.value).subscribe({
			next: () => this.successMessage = true,
			error: () => this.failMessage = true,
			complete: () => {
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.router.navigateByUrl('/');
					}
					this.timer--;
				}, 1000);
			}
		})
	}
}
