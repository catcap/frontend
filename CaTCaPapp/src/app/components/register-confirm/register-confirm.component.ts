import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
	selector: 'app-register-confirm',
	templateUrl: './register-confirm.component.html',
	styleUrls: ['./register-confirm.component.scss']
})
export class RegisterConfirmComponent implements OnInit, AfterViewInit {
	feedback: object | undefined;
	timer: number = 5;
	uidb64: string;
	token: string;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private authService: AuthenticationService,
	) { }

	ngOnInit(): void {
		this.uidb64 = this.route.snapshot.paramMap.get('uidb64');
		this.token = this.route.snapshot.paramMap.get('token');
	}

	ngAfterViewInit(): void {
		this.authService.confirmRegister(this.uidb64, this.token).subscribe({
			next: () => this.feedback = { status: true, message: 'Inscription validée' },
			error: () => {
				this.feedback = { status: false, message: 'Échec de la validation de l\'inscription' }
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.router.navigateByUrl('/');
					}
					this.timer--;
				}, 1000)
			},
			complete: () => {
				const interval = setInterval(() => {
					if (this.timer === 0) {
						clearInterval(interval);
						this.router.navigateByUrl('/');
					}
					this.timer--;
				}, 1000)
			}
		});
	}
}
