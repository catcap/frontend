import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

import Message from 'src/app/defs/interfaces/message';
import { DataService } from 'src/app/services/data/data.service';
import { WebsocketService } from 'src/app/services/websocket/websocket.service';

@Component({
	selector: 'app-notification',
	templateUrl: './notification.component.html',
	styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {
	@Input() useremail: string;
	faBell = faBell as IconProp;
	received: Message[] = [];
	bell: boolean = false;
	incoming: object[] = [];
	toggle: boolean = false;
	private webSocketSubscription: Subscription;

	constructor(
		private webSocketService: WebsocketService,
		private dataService: DataService,
	) {
		if (this.useremail && !this.webSocketSubscription) {
			this.webSocketService.connectWebSocket(this.useremail);
			this.webSocketSubscription = this.webSocketService.getMessages().subscribe({
				next: (res) => {
					this.notificationTreatment(res['message']);
				}
			})
		}
	}

	expand(): void {
		this.toggle = !this.toggle;
		if (this.toggle) this.incoming = [];
	}

	notificationTreatment(notification: string): void {
		const message: Message = {
			content: '',
			uid: notification.split("_#_")[0],
		};
		if (notification.split("_#_")[1].startsWith("DEL_")) {
			const notif = notification.split("DEL_")[1];
			const data = notif.match(/<([^>]*)>/);
			message.content = `Le carte ${data[1]} a été supprimé`;
		} else if (notification.split("_#_")[1].startsWith("OK_")) {
			const notif = notification.split("OK_")[1];
			const data = notif.match(/<([^>]*)>/);
			message.content = `La carte ${data[1]} a été enregistrée`;
		} else if (notification.split("_#_")[1].startsWith("GP_")) {
			const notif = notification.split("GP_")[1];
			const data = notif.match(/<([^>]*)>/);
			if (/create_.+/.test(notif)) {
				message.content = `Création du groupe ${data[1]}`
			}
			if (/add_.+/.test(notif)) {
				message.content = `Vous avez été ajouté au groupe ${data[1]}`
			}
			if (/leave_.+/.test(notif)) {
				message.content = `Vous avez quitté le groupe ${data[1]}`
			}
			if (/join_.+/.test(notif)) {
				message.content = `Vous avez rejoint le groupe ${data[1]}`
			}
			if (/ban_.+/.test(notif)) {
				message.content = `Vous avez été exclu du groupe ${data[1]}`
			}
			if (/del_.+/.test(notif)) {
				message.content = `Le groupe ${data[1]} a été supprimé`
			}
		} else if (notification.split("_#_")[1].startsWith("SER_")) {
			const notif = notification.split("SER_")[1];
			const data = [...notif.matchAll(/<([^>]*)>/g)];
			const dateStart = new Date(data[0][1]);
			const dateEnd = new Date(data[1][1]);
			const start = {
				date: `${dateStart.getDate().toString().padStart(2, '0')}/${(dateStart.getMonth() +1).toString().padStart(2, '0')}/${dateStart.getFullYear()}`,
				hour: `${dateStart.getHours().toString().padStart(2, '0')}:${dateStart.getMinutes().toString().padStart(2, '0')}`
			}
			const end = {
				date: `${dateEnd.getDate().toString().padStart(2, '0')}/${(dateEnd.getMonth() +1).toString().padStart(2, '0')}/${dateEnd.getFullYear()}`,
				hour: `${dateEnd.getHours().toString().padStart(2, '0')}:${dateEnd.getMinutes().toString().padStart(2, '0')}`
			}
			message.content = `L'application sera en maintenance du ${start.date} à ${start.hour} jusqu'au ${end.date} à ${end.hour}.`
		} else if (notification.split("_#_")[1].startsWith("DATA_")) {
			const notif = notification.split("DATA_")[1];
			const data = notif.match(/<([^>]*)>/);
			message.content = `Les données ${data[1]} sont prêtes`;
			this.dataService.reload();
		}
		const index = this.incoming.push(message) - 1;
		this.bell = true;
		this.received.unshift(message);
		setTimeout(() => {
			this.incoming.splice(index, 1);
			this.bell = false;
		}, 5000);
	}

	deleteNotification(notification_uid): void {
		let index = this.received.findIndex((el) => el.uid === notification_uid)
		this.received.splice(index, 1);
		index = this.incoming.findIndex((el) => el['uid'] === notification_uid)
		if (index >= 0) this.incoming.splice(index, 1);
		if (this.received.length <= 0) this.bell = false;
		const data = {
			notification_uid: notification_uid,
		}
		this.webSocketService.sendData(data);
	}

	ngOnInit(): void {
		if (this.useremail && !this.webSocketSubscription) {
			this.webSocketService.connectWebSocket(this.useremail);
			this.webSocketSubscription = this.webSocketService.getMessages().subscribe({
				next: (res) => {
					this.notificationTreatment(res['message']);
				}
			})
		}
	}


	ngOnDestroy(): void {
		if (this.webSocketSubscription) {
			this.webSocketSubscription.unsubscribe();
		}
	}
}
