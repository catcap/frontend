import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoutesNames } from 'src/app/defs/enums/routes';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { JwtService } from 'src/app/services/jwt/jwt.service';
import { UserService } from 'src/app/services/user/user.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent {
	loginForm: FormGroup;
	feedback: object | undefined;
	forgotURL: string = RoutesNames.Forgotten_Password;

	constructor(
		private authService: AuthenticationService,
		private userService: UserService,
		private router: Router,
	) {
		this.loginForm = new FormGroup({
			email: new FormControl('', [Validators.required, Validators.email]),
			password: new FormControl('', [Validators.required])
		})
	}

	get email() {
		return this.loginForm.get('email')
	}

	get password() {
		return this.loginForm.get('password')
	}

	onSubmit() {
		this.authService.login(this.loginForm.value).subscribe({
			error: () => {
				this.feedback = { status: true, message: 'Mauvais email et/ou mot de passe' };
			},
			complete: () => {
				this.userService.me().subscribe();
				this.router.navigateByUrl('/');
			},
		})
	}
}
