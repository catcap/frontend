import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addon',
  templateUrl: './addon.component.html',
  styleUrl: './addon.component.scss'
})
export class AddonComponent implements OnInit {
  version: string;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
      this.version = this.route.snapshot.paramMap.get('version');
      this.downloadAddon(this.version);
  }
  
  downloadAddon(filename: string): void {
    const url = `./assets/addon/${filename}`;
    console.log(url)
    const a = document.createElement('a')
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
    window.location.href = '/';
  }
}
