import { FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";

export const endDateValidator: ValidatorFn = (
	control: FormGroup,
): ValidationErrors | null => {
	const start = control.get('start').value;
	const end = control.get('end').value;
	if (!start || !end) return null;
	else if (end <= start) return { minDate: true };
	else return null;
}
