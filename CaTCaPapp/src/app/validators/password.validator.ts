import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const passwordValidator: ValidatorFn = (
	control: AbstractControl,
): ValidationErrors | null => {
	const value: string = control.value;
	if (!/(?=.*[A-Z])/.test(value)) {
		return {uppercase: true};
	}
	if (!/(?=.*[a-z])/.test(value)) {
		return {lowercase: true};
	}
	if (!/(?=.*[0-9])/.test(value)) {
		return { numeric: true };
	}
	if (!/(?=.*[!@#\$%\^&\*\(\)_\-+=?])/.test(value)) {
		return { specialChar: true };
	  }	  
	if (!/.{8,}/.test(value)) {
		return {length: true};
	}
	else {
		return null;
	}
}