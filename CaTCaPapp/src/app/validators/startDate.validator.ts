import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const startDateValidator: ValidatorFn = (
	control: AbstractControl,
): ValidationErrors | null => {
	const now = new Date().toISOString().slice(0, 16);
	const start = control.value;
	if (start && start <= now) return { minDate: true };
	else if (!start) return null;
	return null;
}
