import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { AuthenticationService } from './services/authentication/authentication.service';
import { RoutesNames } from './defs/enums/routes';
import { UserService } from './services/user/user.service';

import { Subscription, filter } from 'rxjs';

import { faAngleDoubleDown, faAngleDoubleUp, faFileShield, faEnvelope, faScaleBalanced } from '@fortawesome/free-solid-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { BreakpointsService } from './services/breakpoints/breakpoints.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
	title = 'CaTCaPapp';
	routes = RoutesNames;
	currentURI: string = '';
	user: object | null = null;
	toggle: boolean = false;
	type: string | null = null;
	disclaimSmalScreen: boolean = true;

	faAngleDoubleDown = faAngleDoubleDown as IconProp;
	faFileShield = faFileShield as IconProp;
	faEnvelope = faEnvelope as IconProp;
	faScaleBalanced = faScaleBalanced as IconProp;

	private subscription: Subscription | undefined;

	@HostListener('window:resize', ['$event'])
	onResize(event: Event): void {
		this.breakpointsService.checkBreakpoints();
	}
	constructor(
		private authService: AuthenticationService,
		private breakpointsService: BreakpointsService,
		private userService: UserService,
		private router: Router,
	) {
		this.userService.me().subscribe();
		this.onResize = this.onResize.bind(this);											
	}
	
	expand(): void {
		this.toggle = !this.toggle;
		if (this.toggle) this.faAngleDoubleDown = faAngleDoubleUp as IconProp;
		else this.faAngleDoubleDown = faAngleDoubleDown as IconProp;
	}

	ngOnInit(): void {
		this.router.events.pipe(
			filter((event) => event instanceof NavigationEnd)
		).subscribe(() => {
			this.currentURI = this.router.url;
		})
		this.subscription = this.breakpointsService.getTypeObservable().subscribe({
			next: (type) => this.type = type
		})
		window.addEventListener('resize', this.onResize);
		this.userService.user$.subscribe({
			next: (user) => this.user = user,
		})
	}

	ngOnDestroy(): void {
		this.authService.logout().subscribe();
		this.subscription.unsubscribe();
	}


}
