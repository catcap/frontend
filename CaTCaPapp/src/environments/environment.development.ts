export const environment = {
	host: 'http://127.0.0.1:4200/#/',
	API: 'http://127.0.0.1:8000/api/',
	WS: 'ws://127.0.0.1:8000/ws/',
};
