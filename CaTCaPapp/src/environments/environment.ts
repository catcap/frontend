export const environment = {
    host: 'https://catcap.univ-smb.fr/#/',
    API: 'https://catcap.univ-smb.fr/api/',
    WS: 'wss://catcap.univ-smb.fr/ws/',
};
